var stompClient = null;
const Wyloguj = document.querySelector('#Wyloguj');
let innertext = '';


Wyloguj.addEventListener('click', () => {
    disconnect();
});
window.addEventListener('load', connect);


function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
}

function connect() {
    let innerContext = document.getElementById('contextpath').innerText;
    var socket = new SockJS(innerContext + '/websocket-example');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        var dest = document.getElementById('gr-id').innerText;

        stompClient.subscribe('/topic/messenger', function (listOfMessages) {
            showGreeting(JSON.parse(listOfMessages.body));
        });

        stompClient.subscribe('/topic/user-list/' + dest, function (usersList) {
            showUsers(JSON.parse(usersList.body));
        });

        stompClient.subscribe('/topic/href/' + dest, function (href) {
            changeHref(JSON.parse(href.body));
        });
        stompClient.subscribe('/topic/checkbox/' + dest, function (checkBox) {
            TriggerTipOrAnswerS(JSON.parse(checkBox.body));
        });

        stompClient.subscribe('/topic/quiz/' + dest, function (quizId) {
            showQuizButton(JSON.parse(quizId.body));
        });
        stompClient.subscribe('/topic/quizResults/'+ dest, function (quizResultsAllStudents) {
            updateResults(JSON.parse(quizResultsAllStudents.body));
        });
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}


function sentText() {
    let dest = document.getElementById('gr-id').innerText;
    let role = document.getElementById("role").innerText;

    stompClient.send("/app/messenger", {}, JSON.stringify({
        'groupId': dest,
        'login': loggedInUser.innerText,
        'text' : $("#input_tekst_for_window_okno").val() ,
        'role': role
    }));
    setTimeout(function () {
        $("#input_tekst_for_window_okno").val('');
    },500)
}

function showGreeting(listOfMessages) {
    $(".tablesMSG").empty();
    for (let i = 0; i < listOfMessages.length; i++) {
        let divRow = document.createElement("div");
        divRow.setAttribute('class','row no-gutters');
        let divDate = document.createElement("div");
        divDate.setAttribute('id','date');
        divDate.innerText=(listOfMessages[i].localDateTime+' ');
        divDate.setAttribute('class','col-2');
        let divLogin = document.createElement("div");
        let idMsg='';

        if (listOfMessages[i].role=='TEACHER'){
            idMsg='loginTeacher'
        }
        else{
            idMsg='loginStudent'
        }
        divLogin.setAttribute('id',idMsg);
        divLogin.innerText=listOfMessages[i].login+': ';
        divLogin.setAttribute('class','col-sm-auto');

        let divText = document.createElement("div");
        divText.setAttribute('id','textMSG');
        divText.innerText=listOfMessages[i].text;
        divText.setAttribute('class','col-7');

        divRow.insertAdjacentElement('afterbegin',divText);
        divRow.insertAdjacentElement('afterbegin',divLogin);
        divRow.insertAdjacentElement('afterbegin',divDate);

        $(".tablesMSG").append(divRow);
    }
    $("#MessengerWindow").scrollTop($("#MessengerWindow")[0].scrollHeight);

}

$(function () {
    $("kom").on('submit', function (e) {
        e.preventDefault();
    });
    $("#disconnect").click(function () {
        disconnect();
    });
    $("#send").click(function () {
        sentText();
    });
});

const activeUsers = document.getElementById('username-table');
let loggedInUser = document.getElementById("loggedInUser");

function showUsers(users) {
    console.log(users);
    let role = document.getElementById("role").innerText;
    let BadgeStatus = '';
// <tr><th>" + users.groupName + "</th></tr>
    var text = "";
    for (var i = 0; i < users.teachers.length; i++) {
        text += "<tr class='teacher-username'>" +
            "<td>" + users.teachers[i] + "</td></tr>";
    }
    for (var i = 0; i < users.usertest.length; i++) {
        BadgeStatus = users.usertest[i].test_status;
        console.log(BadgeStatus);
        let BadgeClass = '';
        let style = null;
        if (role === 'STUDENT') {
            style = "style='display:none'";
        }

        if (BadgeStatus === 'no_test') {
            BadgeClass = "class= 'badge badge-pill badge-secondary'";

        } else if (BadgeStatus === 'gotowe') {
            BadgeClass = "class= 'badge badge-pill badge-warning'";

        } else if (BadgeStatus === 'w toku') {
            BadgeClass = "class= 'badge badge-pill badge-info'";

        } else if (BadgeStatus === 'zakończone') {
            BadgeClass = "class= 'badge badge-pill badge-success'";
        }
        text += "<tr class='student-username'>" +
            "<td>" + users.usertest[i].username + "</td>" +
            "<td>" + "<span " + BadgeClass + " name='BadgeUserWindow' id ='BadgeUserWindow '" + style + ">" + BadgeStatus + "</span>" + "</td>" +
            "</tr>";
    }
    activeUsers.innerHTML = text;
    console.log(activeUsers.innerHTML)
}

// uaktualnia link do zadania
function changeHref(href) {
    let ButtonStartTest = document.getElementById("ButtonStartTest");
    ButtonStartTest.setAttribute('href', href.href);
    if (href.href !== null) {
        ButtonStartTest.style.display = 'block';
    } else {
        ButtonStartTest.style.display = 'none';
    }
}


function buttonTerminateTest() {
    let dest = document.getElementById('gr-id').innerText;
    let username = document.getElementById('username').innerText;

    // uaktualnia status dla wszystkich
    stompClient.send("/app/allstatus/" + dest, {}, JSON.stringify({
        'groupId': dest,
        'username': username,
        'test_status': 'no_test'
    }));
    // kończy zadanie dla wszystkich
    stompClient.send("/app/href/" + dest, {}, JSON.stringify({'href': null, 'groupID': dest}));

    let ButtonStartTest = document.getElementById("ButtonStartTest");
    ButtonStartTest.style.display = 'none';
    window.location.href = "/index";
    let ButtonTerminateTest = document.getElementById("ButtonTerminateTest");
    ButtonTerminateTest.style.display = 'none';
}


function buttonchosentest(href) {
    let role = document.getElementById("role");
    if (role.innerText === 'TEACHER') {
        let dest = document.getElementById('gr-id').innerText;
        let url = document.createElement('a');
        url.setAttribute('href', href);
        let param = url.search.substr(1);
        // uaktualnia status dla wszystkich
        stompClient.send("/app/allstatus/" + dest, {}, JSON.stringify({
            'groupId': dest,
            'username': loggedInUser.innerText,
            'test_status': 'gotowe'
        }));

        // uaktualnia link
        stompClient.send("/app/href/" + dest, {}, JSON.stringify({'href': url.pathname+'?'+param, 'groupID': dest}));
    }
};

function buttonStartTest() {
    let dest = document.getElementById('gr-id').innerText;
    let username = document.getElementById('username').innerText;

    // uaktualnia status poszczególnego studenta
    stompClient.send("/app/personalstatus/" + dest, {}, JSON.stringify({
        'groupId': dest,
        'username': username,
        'test_status': 'w toku'
    }));
};

function buttonFinishTask() {
    let dest = document.getElementById('gr-id').innerText;
    let username = document.getElementById('username').innerText;

    // uaktualnia status poszczególnego studenta
    stompClient.send("/app/personalstatus/" + dest, {}, JSON.stringify({
        'groupId': dest,
        'username': username,
        'test_status': 'zakończone'
    }));
}



function showQuizButton(quizId) {
    let ButtonStartTest = document.getElementById("ButtonStartQuiz");
    if(quizId === -1){
        ButtonStartTest.setAttribute('class', "btn btn-lg btn-info btn-block disabled");
        ButtonStartTest.setAttribute('aria-disabled', "true");
        window.location.href = '/index';
    }
    else{
        ButtonStartTest.setAttribute('href', '/showQuiz?id=' + quizId);
        ButtonStartTest.setAttribute('class', "btn btn-lg btn-danger btn-block active");
        ButtonStartTest.setAttribute('aria-disabled', "false");
    }
}


function updateResults(quizResultsAllStudents){
    console.log(quizResultsAllStudents);
    let studentResultsList = document.getElementById("studentsResults");
    let text = "<tr>\n" +
        "                        <th style=\"width: 30%\">Student</th>\n" +
        "                        <th style=\"width: 10%\">A</th>\n" +
        "                        <th style=\"width: 10%\">B</th>\n" +
        "                        <th style=\"width: 10%\">C</th>\n" +
        "                        <th style=\"width: 10%\">D</th>\n" +
        "                        <th style=\"width: 30%\">Result</th>\n" +
        "                    </tr><tbody>\n";
        for(var i = 0; i < quizResultsAllStudents.results.length; i++) {
            text += "<tr>\n" + "<td>"+ quizResultsAllStudents.results[i].username +"</td>\n";

            if(quizResultsAllStudents.results[i].answerA != null){
                if(quizResultsAllStudents.results[i].answerA){text += "<td style='background-color: #8dd17f'>OK</td>\n"}
                else {text += "<td style='background-color: #e79f9d'>NOPE</td>\n"}
            }
            else {text += "<td style='background-color: #e2e2e2'></td>\n"}

            if(quizResultsAllStudents.results[i].answerB != null){
                if(quizResultsAllStudents.results[i].answerB){text += "<td style='background-color: #8dd17f'>OK</td>\n"}
                else {text += "<td style='background-color: #e79f9d'>NOPE</td>\n"}
            }
            else {text += "<td style='background-color: #e2e2e2'></td>\n"}

            if(quizResultsAllStudents.results[i].answerC != null){
                if(quizResultsAllStudents.results[i].answerC){text += "<td style='background-color: #8dd17f'>OK</td>\n"}
                else {text += "<td style='background-color: #e79f9d'>NOPE</td>\n"}
            }
            else {text += "<td style='background-color: #e2e2e2'></td>\n"}

            if(quizResultsAllStudents.results[i].answerD != null){
                if(quizResultsAllStudents.results[i].answerD){text += "<td style='background-color: #8dd17f'>OK</td>\n"}
                else {text += "<td style='background-color: #e79f9d'>NOPE</td>\n"}
            }
            else {text += "<td style='background-color: #e2e2e2'></td>\n"}

            if(quizResultsAllStudents.results[i].result != null){
                if(quizResultsAllStudents.results[i].result){text += "<td style='background-color: #8dd17f'>OK</td>\n"}
                else {text += "<td style='background-color: #e79f9d'>NOPE</td>\n"}
            }
            else {text += "<td style='background-color: #e2e2e2'></td>\n"}

            text += "</tr>"
            }

            text += "</tbody><tfoot><tr><td></td><td></td><td></td><td></td><td></td><td>Wynik: " + quizResultsAllStudents.percentage +"%</td></tr></tfoot>";
            studentResultsList.innerHTML = text;
}

function changeGroup(){
    var groupId = document.getElementById("groupSelect").value;
    window.location.href = '/changeGroup/' + groupId;
}

function changeCategoryQuiz() {
    var categoryName = document.getElementById("categoryQuizSelect").value;
    var taskType = document.getElementById("materialTypeSelect").value;
    if(taskType != null) {
        window.location.href = '/showQuizzes?category=' + categoryName;
    }
}

function changeTaskList() {
    var categoryName = document.getElementById("categoryQuizSelect").value;
    var taskType = document.getElementById("materialTypeSelect").value;
    var showFavourites = document.getElementById("showFavourites").checked;
    if(taskType != null){
    window.location.href = 'showMaterials?type=' + taskType + '&category=' + categoryName + '&favourites=' + showFavourites;
    }
}

function addNewMaterial(){
    var taskType = document.getElementById("materialTypeSelect").value;
    if(taskType != null){
        if(taskType === 'ZADANIE'){
            window.location.href = '/addtask';
        }
        else if(taskType === 'QUIZ'){
            window.location.href = '/addQuiz';
        }}
}
    function showQuizzesAndTasks() {
        window.location.href = '/showMaterials?type=QUIZ&category=ALL&favourites=false';
    }
    function showLessonMaterials() {
        window.location.href='/showLessonMaterials?type=personal';
    }

    function showCodeSnippetTextarea(invisible) {
    var codeSnippetTextarea = document.getElementById("codeSnippetTextarea");

    if(invisible){
        codeSnippetTextarea.setAttribute("style","display: block");
    }
    else{
        codeSnippetTextarea.setAttribute("style","display: none");
    }

    }




