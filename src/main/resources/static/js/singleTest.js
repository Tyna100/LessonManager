window.addEventListener('load', TriggerTipOrAnswer);

function TriggerTipOrAnswer(){
    let checkTip=document.getElementById("wskazowkaCheckBox").checked;
    let checkAnswer=document.getElementById("rozwiazanieCheckBox").checked;
    let wskazowka = document.getElementById("wskazowka");
    let rozwiazanie = document.getElementById("rozwiazanie_zadania");

    if (checkTip){
        wskazowka.style.color='#000000'
    }
    else {
        wskazowka.style.color='#D3D3D3';
    }
    if (checkAnswer){
        rozwiazanie.style.color='#000000'
    }
    else {
        rozwiazanie.style.color='#D3D3D3';
    }
    let dest = document.getElementById('gr-id').innerText;
    // uaktualnia wskazówkę i rozwiązanie
    stompClient.send("/app/checkbox/" + dest, {}, JSON.stringify({
        'groupId': dest,
        'isTipOn': checkTip,
        'isAnswerOn': checkAnswer
    }));
}

function TriggerTipOrAnswerS(checkBox){
   let rozwiazanieStudent = document.getElementById("rozwiazanie_zadaniaS");
    let wskazowkaStudent = document.getElementById("wskazowkaS");
    let labelWskazowka=document.getElementById("labelWskazowka");
    let labelRozwiazanie=document.getElementById("labelRozwiazanie");

    if (checkBox.isTipOn) { wskazowkaStudent.style.display = 'block'; labelWskazowka.style.display='block'; }
    else {  wskazowkaStudent.style.display = 'none'; labelWskazowka.style.display='none'; }
    if (checkBox.isAnswerOn) {rozwiazanieStudent.style.display = 'block'; labelRozwiazanie.style.display='block'; }
    else {
        rozwiazanieStudent.style.display = 'none'; labelRozwiazanie.style.display='none'; }
}
function goBack() {
    window.history.back();
}