package com.sdc.LessonManager.config;


import com.sdc.LessonManager.filter.LoginFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.util.Locale;

@ComponentScan(basePackages = {"com.sdc.LessonManager.controllers"})

@Configuration
@EnableWebMvc
public class WebConfig extends WebMvcConfigurerAdapter implements WebMvcConfigurer {

    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("forward:/menu");

        registry.addViewController("/index").setViewName("index");
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/menu").setViewName("menu");
        registry.addViewController("/addcategory").setViewName("addCategoryPage");
        registry.addViewController("/form").setViewName("form");
        registry.addViewController("/addtask").setViewName("addTaskPage");
        registry.addViewController("/showTasks").setViewName("showTasksPage");


        registry.addViewController("/addQuiz").setViewName("addQuizPage");
        registry.addViewController("/showQuizzes").setViewName("showQuizzesPage");


        registry.addViewController("/addQuiz").setViewName("addQuizPage");
        registry.addViewController("/showQuizzes").setViewName("showQuizzesPage");
        registry.addViewController("/showQuiz").setViewName("showQuizPage");
        registry.addViewController("/showQuiz").setViewName("showQuiz");
        registry.addViewController("/shareQuiz").setViewName("showQuizResultsAllStudents");
        registry.addViewController("/shareQuiz").setViewName("showQuizResultsAllStudentsPage");
        registry.addViewController("/checkQuiz").setViewName("showQuizResultsPage");

        registry.addViewController("/showLessonMaterials").setViewName("showLessonMaterialsPage");


    }
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(
                "/webjars/**",
                "/img/**",
                "/css/**",
                "/templates/fragments_index/**",
                "/js/**")
                .addResourceLocations(
                        "classpath:/META-INF/resources/webjars/",
                        "classpath:/static/img/",
                        "classpath:/static/css/",
                        "classpath:/static/templates/fragments_index/",
                        "classpath:/static/js/");
    }

    //zmiana locale
    @Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(Locale.UK);
        return slr;
    }

    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("lang");
        return lci;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(localeChangeInterceptor());
    }

    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource
                = new ReloadableResourceBundleMessageSource();

        messageSource.setBasename("classpath:messages");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

    @Bean
    public LocalValidatorFactoryBean getValidator() {
        LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
        bean.setValidationMessageSource(messageSource());
        return bean;
    }

    @Bean FilterRegistrationBean<LoginFilter> loginFilter(){
        FilterRegistrationBean<LoginFilter> filterRegistrationBean = new FilterRegistrationBean<>();
        filterRegistrationBean.setFilter(new LoginFilter());
        filterRegistrationBean.addUrlPatterns("/login");
        return filterRegistrationBean;
    }
}