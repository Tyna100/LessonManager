package com.sdc.LessonManager.active_users;

import com.sdc.LessonManager.model.entity_IndexController.UserTest;
import com.sdc.LessonManager.model.quiz.QuizAnswersForm;
import com.sdc.LessonManager.register_login_logout.model.CourseRole;
import com.sdc.LessonManager.register_login_logout.model.User;
import com.sdc.LessonManager.services.GroupService;
import com.sdc.LessonManager.services.UserTestService;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
public class ActiveUsersService {

    private final GroupService groupService;
    private final UserTestService userTestService;
    public static Map<Long, List<User>> map;
    private Map<String,List<QuizAnswersForm>> quizResultsMap;

    public void addActiveUser(User user){
        Long activeGroupID = user.getActiveGroupId();
        userTestService.saveInitialUser(user.getUsername());

        map.get(activeGroupID).add(user);
        updateActiveUsers(activeGroupID);
        System.out.println("User added : " + user.getUsername());
    }

    public void removeActiveUser(User user){
        Long activeGroupID = user.getActiveGroupId();
        userTestService.deleteUser(user.getUsername());
        map.get(activeGroupID).remove(user);
        updateActiveUsers(activeGroupID);
        System.out.println("User removed : " + user.getUsername());
    }

    public void updateActiveUser(User user, Long oldGroupId){
        Long activeGroupID = user.getActiveGroupId();
        map.get(oldGroupId).remove(user);
        map.get(activeGroupID).add(user);

        updateActiveUsers(activeGroupID);
        updateActiveUsers(oldGroupId);
    }

    public List<User> getActiveUsersByGroupId(Long groupId) {
        return map.get(groupId)
                .stream()
                .distinct()
                .collect(Collectors.toList());
    }

    public List<User>getActiveStudentsByGroupId(Long groupId){
        return getActiveUsersByGroupId(groupId).stream()
                .filter(user -> user.getCourseRole() == CourseRole.STUDENT)
                .collect(Collectors.toList());
    }


    private final SimpMessagingTemplate template;

    public void updateActiveUsers(Long groupId){
        String destination = String.format("/topic/user-list/%s",groupId);
        template.convertAndSend(destination,getActiveUsersPackage(groupId));
    }


    public ActiveUsersPackage getActiveUsersPackage(Long groupId){

        List<String> teachers = getActiveUsersByGroupId(groupId).stream()
                .filter(user -> user.getCourseRole() == CourseRole.TEACHER)
                .map(user->user.getUsername()).collect(Collectors.toList());


        List<String> students = getActiveUsersByGroupId(groupId).stream()
                .filter(user -> user.getCourseRole() == CourseRole.STUDENT)
                .map(user->user.getUsername()).collect(Collectors.toList());

        String groupName =groupService.findById(groupId).getName();

        List<UserTest> usertest =userTestService.findAll().stream()
                .filter(user-> students.contains(user.getUsername()))
                .collect(Collectors.toList());

        return new ActiveUsersPackage(groupName,teachers,students,usertest);
    }

    public void initQuizForGroup(Long quizId, Long groupId){
        String groupQuizName =  groupId + " " + quizId;
        List<QuizAnswersForm> quizAnswersFormList = new ArrayList<>();
        getActiveStudentsByGroupId(groupId).forEach(user -> quizAnswersFormList.add(new QuizAnswersForm(user.getUsername())));
        quizResultsMap.put(groupQuizName, quizAnswersFormList);
    }

    private final SimpMessagingTemplate resultMessageTemplate;

    public void updateQuizForGroup(String groupQuizName,QuizAnswersForm quizAnswersForm){
        Optional<QuizAnswersForm> q = quizResultsMap.get(groupQuizName).stream()
                .filter(quizResults -> quizResults.getUsername().equals(quizAnswersForm.getUsername()))
                .findFirst();
        if(q.isPresent()){
            QuizAnswersForm quizResults = q.get();
            quizResults.setAnswerA(quizAnswersForm.getAnswerA());
            quizResults.setAnswerB(quizAnswersForm.getAnswerB());
            quizResults.setAnswerC(quizAnswersForm.getAnswerC());
            quizResults.setAnswerD(quizAnswersForm.getAnswerD());
            quizResults.setResult(quizAnswersForm.getResult());

            Long goodAnswers = quizResultsMap.get(groupQuizName).stream().filter(form-> form.getResult()).count();
            Long numberOfAnswers = quizResultsMap.get(groupQuizName).stream().filter(form-> form.getResult()!= null).count();

            String groupId = groupQuizName.split(" ")[0].trim();
            String destination = String.format("/topic/quizResults/%s",groupId);
            System.out.println(destination);
            resultMessageTemplate.convertAndSend(destination,new QuizResults(quizResultsMap.get(groupQuizName),(goodAnswers/numberOfAnswers)*100));
            System.out.println(quizResultsMap.get(groupQuizName));
        }
    }

    public void removeQuiz(String groupQuizName){
        quizResultsMap.remove(groupQuizName);
    }

    //@PostConstruct
//    private Map<Long, List<User>> populateMap(){
//        map = new HashMap<>();
//        Iterable<Grupa> groups = groupService.findAll();
//        groups.forEach(group -> map.put(group.getId_grupa(), new ArrayList<>()));
//
//        return map;
//    }
    @PostConstruct
    private void initMap(){quizResultsMap = new HashMap<>();
    }

}
