package com.sdc.LessonManager.active_users;

import com.sdc.LessonManager.model.entity_IndexController.UserTest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
@AllArgsConstructor
public class ActiveUsersPackage {
    private String groupName;
    private List<String> teachers;
    private List<String> students;
    private List<UserTest> usertest;
}
