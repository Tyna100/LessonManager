package com.sdc.LessonManager.active_users;

import com.sdc.LessonManager.model.quiz.QuizAnswersForm;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuizResults {
    private List<QuizAnswersForm> results;
    private Long percentage;
}
