package com.sdc.LessonManager.register_login_logout.validator;

import com.sdc.LessonManager.register_login_logout.model.User;
import com.sdc.LessonManager.register_login_logout.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
@RequiredArgsConstructor
public class UserRegistrationDataValidator implements Validator {

    private final UserService userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        User user = (User) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"username","NotEmpty");
        if(user.getUsername().length() < 6 || user.getUsername().length() > 32){
            errors.rejectValue("username", "Size.user.username");
        }
        if(userService.findByUserName(user.getUsername()) != null){
            errors.rejectValue("username", "Duplicate.user.username");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"password","NotEmpty");
        if(user.getPassword().length() < 6 || user.getUsername().length() > 32){
            errors.rejectValue("password", "Size.user.password");
        }
        if(!user.getPasswordConfirm().equals(user.getPassword())){
            errors.rejectValue("passwordConfirm", "Diff.user.passwordConfirm");
        }
        if(user.getGroups().isEmpty()){
            errors.rejectValue("groups","Groups.notEmpty");
        }
    }
}
