package com.sdc.LessonManager.register_login_logout.service;

import com.sdc.LessonManager.controllers.IndexControllers.Util.UtilClass;
import com.sdc.LessonManager.model.entity_IndexController.Grupa;
import com.sdc.LessonManager.model.quiz.Quiz;
import com.sdc.LessonManager.register_login_logout.model.Role;
import com.sdc.LessonManager.register_login_logout.model.User;
import com.sdc.LessonManager.register_login_logout.model.UserForm;
import com.sdc.LessonManager.register_login_logout.model.repository.RoleRepository;
import com.sdc.LessonManager.register_login_logout.model.repository.UserRepository;
import com.sdc.LessonManager.services.GroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final BCryptPasswordEncoder encoder;

    private final GroupService groupService;

       public void save(User user) {

           user.setRoles(new HashSet<>(roleRepository.findAll()));
           user.setPassword(encoder.encode(user.getPassword()));

           userRepository.save(user);
    }

    public User findByUserName(String username) {
        return userRepository.findByUsername(username);
    }

    public User findById(Long id){ return userRepository.findById(id).orElseThrow(()->new IllegalArgumentException("User with such id doesn't exist."));}


    public List<User> findAll() {
        return userRepository.findAll();
    }


    public void addRoles(String... roleNames){
        Arrays.asList(roleNames).forEach(roleName ->{
            Role role = new Role();
            role.setName(roleName);
            roleRepository.save(role);
        });

    }

    public void updateUser(Long id, UserForm user, List<Grupa> groups){
            User u = findById(id);
            u.setActiveGroupId(user.getActiveGroupId());
            u.setUsername(user.getUsername());
            u.setPassword(user.getPassword());
            u.setPasswordConfirm(user.getPasswordConfirm());
            u.setCourseRole(user.getCourseRole());
            u.setGroups(groups);
            userRepository.save(u);
            UtilClass.updateLoggedInUser(u);
        }

        public void updateUsersFavouriteQuizzes(Long userId, List<Quiz> quizzes){
           User user = findById(userId);
           user.setFavouriteQuizzes(quizzes);
           userRepository.save(user);
        }

        public void updateUserActiveGroup(Long userId, Long groupId){
           User u = findById(userId);
           if(u.getGroups().contains(groupService.findById(groupId))){
           u.setActiveGroupId(groupId);
           userRepository.save(u);
           }
        }

}
