package com.sdc.LessonManager.register_login_logout.controller;


import com.sdc.LessonManager.active_users.ActiveUsersService;
import com.sdc.LessonManager.register_login_logout.model.CourseRole;
import com.sdc.LessonManager.register_login_logout.model.User;
import com.sdc.LessonManager.register_login_logout.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequiredArgsConstructor
public class LoginLogoutController {
    private final UserService userService;
    private final ActiveUsersService activeUsersService;
    @RequestMapping(value = "/user-redirect")
    public String redirectHomePage(HttpServletRequest request, Authentication authentication){

        String userName = authentication.getName();
        User user = userService.findByUserName(userName);
        HttpSession session = request.getSession(false);
        session.setAttribute("appUser", user);

        activeUsersService.addActiveUser(user);

        String roleUrl = ((user.getCourseRole() == CourseRole.TEACHER) ? "home-t" : "home-s");
        return String.format("redirect:/%s",roleUrl);
    }


    @RequestMapping(value = "/login-error")
    public String loginError(Model model){

        System.out.println("Login fail");
        model.addAttribute("loginError",true);
        return "login";
    }

}


