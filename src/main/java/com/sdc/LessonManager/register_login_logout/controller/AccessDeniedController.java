package com.sdc.LessonManager.register_login_logout.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AccessDeniedController {

    @GetMapping("/accessDenied")
    public String showAccessDeniedPage(){
        return "accessDenied";
    }


}
