package com.sdc.LessonManager.register_login_logout.controller;

import com.sdc.LessonManager.active_users.ActiveUsersService;
import com.sdc.LessonManager.controllers.IndexControllers.Util.UtilClass;
import com.sdc.LessonManager.model.entity_IndexController.Grupa;
import com.sdc.LessonManager.register_login_logout.model.CourseRole;
import com.sdc.LessonManager.register_login_logout.model.User;
import com.sdc.LessonManager.register_login_logout.model.UserForm;
import com.sdc.LessonManager.register_login_logout.service.UserService;
import com.sdc.LessonManager.register_login_logout.validator.UserRegistrationDataValidator;
import com.sdc.LessonManager.services.GroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class UserFormController {
    @InitBinder
    public void initBinder(WebDataBinder binder){
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    private final UserService userService;

    private final GroupService groupService;

    private final UserRegistrationDataValidator validator;

    private final ActiveUsersService activeUsersService;

    @GetMapping("/editUser")
    public String showEditUser(Model model){
        UserForm form = new UserForm(UtilClass.getLoggedInUser());
        model.addAttribute("user", form);
        addModelAttributes(model);
        return "editUser";
    }

    @PostMapping("/editUser")
    public String saveEditUser(@Valid @ModelAttribute("user")UserForm userForm,
                               @RequestParam(value = "grps", required = false)Long[] chosenGroupsIds,
                               BindingResult bindingResult, Model model){
        addModelAttributes(model);

        List<Grupa> chosenGroups = new ArrayList<>();
        if(chosenGroupsIds != null){
            for(Long id: chosenGroupsIds){
                Grupa grupa = groupService.findById(id);
                chosenGroups.add(grupa);
            }
        }
        if(bindingResult.hasErrors()){
            return "editUser";
        }
        if(!userForm.getPasswordConfirm().equals(userForm.getPassword())){
            bindingResult.rejectValue("passwordConfirm", "Diff.user.passwordConfirm");
        }
        userService.updateUser(UtilClass.getLoggedInUser().getId(),userForm,chosenGroups);
        User user = UtilClass.getLoggedInUser();
        model.addAttribute("loggedInUser",user);
        activeUsersService.updateActiveUser(user,user.getActiveGroupId());
        return "redirect:/index";
    }


    @PostMapping(value = "/form")
    public String addUser(@ModelAttribute("user") @Valid User newUser,
                          @RequestParam(value = "grps", required = false)Long[] chosenGroupsIds,
                          BindingResult bindingResult, Model model){

        addModelAttributes(model);

        List<Grupa> chosenGroups = new ArrayList<>();
        if(chosenGroupsIds != null){
            for(Long id: chosenGroupsIds){
                Grupa grupa = groupService.findById(id);
                chosenGroups.add(grupa);
            }
        }
        newUser.setGroups(chosenGroups);

        validator.validate(newUser,bindingResult);

        if(bindingResult.hasErrors()){
            System.out.println("BINDING RESULT ERROR");
            return "form";
        }
        else {
            newUser.setActiveGroupId(chosenGroups.get(0).getId_grupa());
            newUser = (User)model.getAttribute("user");
            model.addAttribute("user", newUser);
            if(newUser!=null) {userService.save(newUser);
//                System.out.println("new user added: " + newUser);
            }
            return "login";

        }

//        userService.addRoles("user");
//        userService.addRoles("admin");

    }

    @GetMapping(value = "/form")
    public String showForm(Model model){
        User newUser = new User();
        model.addAttribute("user", newUser);
        addModelAttributes(model);
        return "form";
    }

    private void addModelAttributes(Model model){
        CourseRole[] courseRoles = CourseRole.values();
        Iterable<Grupa> availableGroups = groupService.findAll();

        model.addAttribute("roles", courseRoles);
        model.addAttribute("availableGroups", availableGroups);
    }

}


