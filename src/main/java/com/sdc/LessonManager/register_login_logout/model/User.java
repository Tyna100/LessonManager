package com.sdc.LessonManager.register_login_logout.model;


import com.sdc.LessonManager.model.entity_IndexController.Grupa;
import com.sdc.LessonManager.model.lesson.Lesson;
import com.sdc.LessonManager.model.lesson.LessonMaterials;
import com.sdc.LessonManager.model.quiz.Quiz;
import lombok.Data;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

@Entity
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private String username;

    @NotNull
    private String password;


    @Transient
    private String passwordConfirm;

    @ManyToMany
    private Set<Role> roles;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private CourseRole courseRole;


    @ManyToMany (cascade = {CascadeType.PERSIST,CascadeType.MERGE},
            fetch = FetchType.EAGER)
    @JoinTable(name = "user_grupa",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "grupa_id"))
    private List<Grupa> groups;


    private Long activeGroupId = 1L;


    @OneToMany(mappedBy = "author")
    private List<Quiz> quizzes;

    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Quiz> favouriteQuizzes;

    @ManyToMany(mappedBy = "students")
    private List<Lesson> studentLessons;

    @OneToMany(mappedBy = "teacher")
    private List<Lesson> teacherLessons;

    @OneToMany (mappedBy = "user")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<LessonMaterials> favouriteLessonMaterials;

    @Transient
    private Boolean isPreparingLessonMaterials;

    @Transient
    private LessonMaterials activeLessonMaterials;

    @Transient
    private Lesson activeLesson;

    @Override
    public String toString() {
        return String.format("id:%s, username: %s, password: %s, passwordConfirm: %s",id, username, password, passwordConfirm);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)return true;

        if(!(obj instanceof User))return false;

        User u = (User) obj;

        return (u.id == id);
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }

    public List<Lesson> getLessons(){
        if(courseRole == CourseRole.STUDENT) return studentLessons;
        return teacherLessons;
    }
}
