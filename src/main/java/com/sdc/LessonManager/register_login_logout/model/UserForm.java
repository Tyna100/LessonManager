package com.sdc.LessonManager.register_login_logout.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class UserForm {

    @NotBlank
    @Email
    @Size(max = 32)
    private String username;

    @NotNull
    @Size(min = 8, max = 32)
    private String password;

    @NotNull
    @Size(min = 8, max = 32)
    private String passwordConfirm;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private CourseRole courseRole;

    List<Long> groupIds;

    private Long activeGroupId = 1L;

    public UserForm(User user){
        this.username = user.getUsername();
        this.password = user.getPassword();
        this.passwordConfirm = user.getPassword();
        this.courseRole = user.getCourseRole();
        this.activeGroupId = user.getActiveGroupId();
        this.groupIds = user.getGroups().stream().map(group-> group.getId_grupa()).collect(Collectors.toList());
    }

}
