package com.sdc.LessonManager.register_login_logout.config;

import com.sdc.LessonManager.active_users.ActiveUsersService;
import com.sdc.LessonManager.register_login_logout.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@RequiredArgsConstructor
public class CustomLogoutHandler implements LogoutHandler {
    private final ActiveUsersService activeUsersService;


    @Override
    public void logout(HttpServletRequest request, HttpServletResponse httpServletResponse,
                       Authentication authentication) {
        HttpSession session = request.getSession(false);
        User user = (User)session.getAttribute("appUser");
        activeUsersService.removeActiveUser(user);
    }
}
