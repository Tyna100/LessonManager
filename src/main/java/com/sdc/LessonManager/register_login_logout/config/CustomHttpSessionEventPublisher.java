package com.sdc.LessonManager.register_login_logout.config;

import com.sdc.LessonManager.active_users.ActiveUsersService;
import com.sdc.LessonManager.register_login_logout.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.security.web.session.HttpSessionEventPublisher;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;

@RequiredArgsConstructor
public class CustomHttpSessionEventPublisher extends HttpSessionEventPublisher {
    private final ActiveUsersService activeUsersService;

    @Override
    public void sessionCreated(HttpSessionEvent event) {
        super.sessionCreated(event);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent event) {
        HttpSession session = event.getSession();
        User user = (User)session.getAttribute("appUser");
        System.out.println("From CustomHttpSessionEventPublisher");
        activeUsersService.removeActiveUser(user);
        super.sessionDestroyed(event);
    }
}
