package com.sdc.LessonManager.register_login_logout.config;

import com.sdc.LessonManager.active_users.ActiveUsersService;
import com.sdc.LessonManager.register_login_logout.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@ComponentScan("com.sdc.LessonManager")
@EnableWebSecurity
@RequiredArgsConstructor
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {


private final ActiveUsersService activeUsersService;
private final UserService userService;
private final BCryptPasswordEncoder encoder;

@Qualifier("userDetailsServiceImpl")
@Autowired
private UserDetailsService userDetailsService;

    @Bean
    public AuthenticationProvider authProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(userDetailsService);
        provider.setPasswordEncoder(encoder);
        return provider;
    }

    @Autowired
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
//        String password = passwordEncoder().encode("password");

        auth
                .authenticationProvider(authProvider());

//                temp
//                .inMemoryAuthentication()
//                .withUser("user")
//                .password(password)
//                .roles("USER")
//                .and()
//                .withUser("admin")
//                .password(password)
//                .roles("ADMIN","USER");
//               temp
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("/resources/**","/webjars/**","/img/**","/h2-console/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http

                .authorizeRequests()
                .antMatchers( "/css/**","/js/**","/form","/h2-console/**").permitAll()
                .antMatchers("/","/menu","/login**","/sessionExpired").permitAll()

                .antMatchers("/deleteQuiz","/editQuiz","/deleteTask").hasRole("TEACHER")

                .anyRequest().authenticated()

                .and()
                .formLogin()
                .loginPage("/login")
                .loginProcessingUrl("/login")
                .usernameParameter("username")
                .passwordParameter("password")

                .failureForwardUrl("/login-error")
                .successHandler(successHandler())
                .permitAll()

                .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .addLogoutHandler(logoutHandler())
                .logoutSuccessUrl("/login?logout=true")
                .permitAll()

                .and()
                .exceptionHandling().accessDeniedPage("/accessDenied");

        http.csrf().disable();
        http.headers().frameOptions().disable();
        http.sessionManagement()
                .maximumSessions(1)
                .expiredUrl("/login?expired=true");

    }


    @Bean
    AuthenticationSuccessHandler successHandler(){return new CustomAuthenticationSuccessHandler(userService,activeUsersService);}

    @Bean
    LogoutHandler logoutHandler(){
        return new CustomLogoutHandler(activeUsersService);}


}
