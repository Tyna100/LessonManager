package com.sdc.LessonManager.filter;

import com.sdc.LessonManager.register_login_logout.model.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)servletRequest;
        HttpSession session = request.getSession(false);
        if(session == null){
            filterChain.doFilter(servletRequest,servletResponse);
        }
        else {
            User appUser = (User)session.getAttribute("appUser");
            if(appUser == null){
                filterChain.doFilter(servletRequest,servletResponse);
            }
            else{
            HttpServletResponse response = (HttpServletResponse) servletResponse;
            response.sendRedirect("/index");
            }
        }
    }
}
