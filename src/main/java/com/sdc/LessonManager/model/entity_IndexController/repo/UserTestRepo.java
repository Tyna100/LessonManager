package com.sdc.LessonManager.model.entity_IndexController.repo;

import com.sdc.LessonManager.model.entity_IndexController.UserTest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserTestRepo extends JpaRepository<UserTest,Long> {
    Optional<UserTest> findByUsername(String username);
    List<UserTest> findAllByGroupId(Long Id);
}
