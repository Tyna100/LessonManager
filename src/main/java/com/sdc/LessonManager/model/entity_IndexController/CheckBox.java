package com.sdc.LessonManager.model.entity_IndexController;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@NoArgsConstructor
@Data
@Entity
public class CheckBox {
    @Id
    private Long groupId;
    private Boolean isTipOn;
    private Boolean isAnswerOn;



    public CheckBox(Long groupId, Boolean isTipOn, Boolean isAnswerOn) {
        this.groupId = groupId;
        this.isTipOn = isTipOn;
        this.isAnswerOn = isAnswerOn;
    }

    public CheckBox(Long groupId, Boolean isTipOn) {
        this.groupId = groupId;
        this.isTipOn = isTipOn;
    }
}
