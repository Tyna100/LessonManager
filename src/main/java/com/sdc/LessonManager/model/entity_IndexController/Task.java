package com.sdc.LessonManager.model.entity_IndexController;

import com.sdc.LessonManager.model.entity_loginController.Kurs;
import com.sdc.LessonManager.model.lesson.LessonMaterials;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Objects;

@Data
@Entity
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idPracyDomowej;

    private Long idgrupa;

    private String kategoria;

    @NotNull
    private String tresc_zadania;

    @NotNull
    @Size(max = 3000)
    private String rozwiazanie_zadania;

    @NotNull
    private String wskazowka;

    private String id_trenera;

    @ManyToOne
    @JoinColumn(name = "id_kurs")
    private Kurs zadania_dla_kursu;

    @ManyToMany(mappedBy = "tasks")
    private List<LessonMaterials> lessonMaterials;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return Objects.equals(idPracyDomowej, task.idPracyDomowej);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPracyDomowej,kategoria,tresc_zadania);
    }
}

