package com.sdc.LessonManager.model.entity_IndexController.repo;

import com.sdc.LessonManager.model.entity_IndexController.Messenger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface MessengerRepo extends JpaRepository<Messenger,Long > {
    ArrayList<Messenger> findAllByGroupId(Long id);
}
