package com.sdc.LessonManager.model.entity_IndexController;

import com.sdc.LessonManager.register_login_logout.model.CourseRole;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import java.util.Objects;

@Data
@Entity
public class UserTest {

    @Id
    private Long Id;
    private String username;
    private Long groupId;
    @Enumerated
    private CourseRole Role;

    private String Test_status="no_test";


    public UserTest() {
    }

    public UserTest(Long groupId,String username,String Test_status ) {
        this.groupId=groupId;
        this.username=username;
        this.Test_status=Test_status;
    }

    public String getTest_status() {
        return Test_status;
    }

    public void setTest_status(String test_status) {
        this.Test_status = test_status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserTest userTest = (UserTest) o;
        return Objects.equals(Id, userTest.Id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Id);
    }
}
