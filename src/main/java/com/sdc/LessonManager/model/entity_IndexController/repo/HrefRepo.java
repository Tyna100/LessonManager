package com.sdc.LessonManager.model.entity_IndexController.repo;

import com.sdc.LessonManager.model.entity_IndexController.Href;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HrefRepo extends JpaRepository<Href,Long> {
    Href findByGroupID(Long id);
}
