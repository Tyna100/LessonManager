package com.sdc.LessonManager.model.entity_IndexController.repo;

import com.sdc.LessonManager.model.entity_IndexController.Task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TaskRepo extends JpaRepository<Task, Long> {
        public List<Task> findByKategoria(String kategoria);
        public Task findByIdPracyDomowej(Long id);
        public List<Task> findAllByKategoriaAndIdgrupa(String kategoria, Long IdGrupa);

}
