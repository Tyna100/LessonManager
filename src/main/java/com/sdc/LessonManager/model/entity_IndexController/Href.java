package com.sdc.LessonManager.model.entity_IndexController;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@NoArgsConstructor
@Data
@Entity
public class Href {
    @Id
    private Long groupID;
    private String href;

    public Href(String href) {
        this.href = href;
    }

    public Href(Long groupID) {
        this.groupID = groupID;
    }

    public Href(String href, Long groupID) {
        this.href = href;
        this.groupID = groupID;
    }
}


