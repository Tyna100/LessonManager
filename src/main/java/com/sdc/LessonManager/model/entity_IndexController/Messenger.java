package com.sdc.LessonManager.model.entity_IndexController;

import com.sdc.LessonManager.register_login_logout.model.CourseRole;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;


@Table
@Data
@Entity
public class Messenger {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long groupId;
    private String login;
    private String text;
    private LocalDateTime localDateTime;
    @Enumerated
    private CourseRole Role;


    public Messenger() {
    }

}
