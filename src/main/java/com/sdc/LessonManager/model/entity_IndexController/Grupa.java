package com.sdc.LessonManager.model.entity_IndexController;


import com.sdc.LessonManager.model.lesson.Lesson;
import com.sdc.LessonManager.register_login_logout.model.User;
import lombok.Data;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class Grupa {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id_grupa;
    String name;

    @ManyToMany (mappedBy = "groups")
    @LazyCollection(LazyCollectionOption.FALSE)
    List<User> users;

    @OneToMany (mappedBy = "group")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Lesson> lessons;

}
