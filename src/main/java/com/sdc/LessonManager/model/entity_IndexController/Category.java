package com.sdc.LessonManager.model.entity_IndexController;


import com.sdc.LessonManager.model.quiz.Quiz;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id_kategoria;
    private String kategoria;

    @OneToMany(mappedBy = "category")
    private List<Quiz> quizzes;

   public Category(String name){
       this.kategoria = name;
   }
}
