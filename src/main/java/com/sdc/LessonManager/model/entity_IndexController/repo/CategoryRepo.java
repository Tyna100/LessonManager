package com.sdc.LessonManager.model.entity_IndexController.repo;

import com.sdc.LessonManager.model.entity_IndexController.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepo extends JpaRepository<Category,Long> {
    Category findByKategoria(String kategoria);
}
