package com.sdc.LessonManager.model.entity_IndexController.repo;


import com.sdc.LessonManager.model.entity_IndexController.CheckBox;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CheckBoxRepo extends JpaRepository<CheckBox,Long> {
    CheckBox findByGroupId(Long id);
}
