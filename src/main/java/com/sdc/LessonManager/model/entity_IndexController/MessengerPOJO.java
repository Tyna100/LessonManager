package com.sdc.LessonManager.model.entity_IndexController;

import com.sdc.LessonManager.register_login_logout.model.CourseRole;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Enumerated;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class MessengerPOJO {
    private Long groupId;
    private String login;
    private String text;
    @Enumerated
    private CourseRole Role;
}
