package com.sdc.LessonManager.model.entity_loginController;

import javax.persistence.*;

@Entity
@Table(name="danelogowania")
public class Login_Password {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    Integer id_logowania;
    @Column
    String login;
    @Column
    String password;
    @OneToOne(mappedBy = "login_password")
    private Kurs kurs;

    public Kurs getKurs() {
        return kurs;
    }

    public void setKurs(Kurs kurs) {
        this.kurs = kurs;
    }

    public Login_Password() {
    }

    public Login_Password( String login, String password) {
        this.id_logowania = id_logowania;
        this.login = login;
        this.password = password;
    }

    public Integer getId_logowania() {
        return id_logowania;
    }

    public void setId_logowania(Integer id_logowania) {
        this.id_logowania = id_logowania;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Login_Password{" +
                "id_logowania=" + id_logowania +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", kurs=" + kurs +
                '}';
    }
}
