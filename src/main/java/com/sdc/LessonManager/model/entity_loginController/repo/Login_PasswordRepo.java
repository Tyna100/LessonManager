package com.sdc.LessonManager.model.entity_loginController.repo;

import com.sdc.LessonManager.model.entity_loginController.Login_Password;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Login_PasswordRepo extends CrudRepository<Login_Password,Integer> {
}
