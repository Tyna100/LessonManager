package com.sdc.LessonManager.model.entity_loginController;


import com.sdc.LessonManager.model.entity_IndexController.Task;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class Kurs {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id_kurs;
    @Column
    private String name;

    @OneToMany(mappedBy = "zadania_dla_kursu", cascade = CascadeType.ALL)
    private List<Task> kursList;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "kurs", unique = true)
    private Login_Password login_password;



    public Login_Password getLogin_password() {
        return login_password;
    }

    public void setLogin_password(Login_Password login_password) {
        this.login_password = login_password;
    }

    public List<Task> getTaskList() {
        if (kursList == null) {
            kursList = new ArrayList<Task>();
        }
        return kursList;
    }

    public void setTaskList(List<Task> kursList) {
        this.kursList = kursList;
    }

    public Kurs() {
    }

    public Kurs(String name) {
        this.name = name;
    }

    public Long getId_kurs() {
        return id_kurs;
    }

    public void setId_kurs(Long id_kurs) {
        this.id_kurs = id_kurs;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Kurs{" +
                "id_kurs=" + id_kurs +
                ", name='" + name + '\'' +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }
}
