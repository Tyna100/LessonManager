package com.sdc.LessonManager.model.entity_loginController.repo;

import com.sdc.LessonManager.model.entity_loginController.Kurs;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KursRepo extends CrudRepository<Kurs,Long> {
}
