package com.sdc.LessonManager.model.quiz;

import com.opencsv.bean.CsvBindByName;
import com.sdc.LessonManager.register_login_logout.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class QuizForm {

    @NotNull
    @CsvBindByName
    private Long categoryId;

//    @CsvBindByName
//    private String categoryName;


    private User author;

    @NotBlank
    @CsvBindByName
    private String question;

    @NotBlank
    @CsvBindByName
    private String answerA;
    @CsvBindByName
    private Boolean isAnswerACorrect;

    @NotBlank
    @CsvBindByName
    private String answerB;
    @CsvBindByName
    private Boolean isAnswerBCorrect;

    @NotBlank
    @CsvBindByName
    private String answerC;
    @CsvBindByName
    private Boolean isAnswerCCorrect;

    @NotBlank
    @CsvBindByName
    private String answerD;
    @CsvBindByName
    private Boolean isAnswerDCorrect;

    @CsvBindByName
    private Boolean hasCodeSnippet;
    @CsvBindByName
    private String codeSnippet;


}
