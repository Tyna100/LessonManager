package com.sdc.LessonManager.model.quiz;

import com.sdc.LessonManager.model.entity_IndexController.Category;
import com.sdc.LessonManager.model.lesson.LessonMaterials;
import com.sdc.LessonManager.register_login_logout.model.User;
import lombok.Data;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Objects;

@Entity
@Data
public class Quiz {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Category category;

    private String question;

    @OneToMany(mappedBy = "quiz")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Answer> answers;

    @ManyToMany(mappedBy = "quizzes")
    private List<LessonMaterials> lessonMaterials;

    @ManyToOne
    private User author;

    @ManyToMany(mappedBy = "favouriteQuizzes")
    private List<User> favouriteForUsers;

    @Transient
    private Boolean isFavouriteForLoggedInUser;

//    @Transient
    private Boolean hasCodeSnippet;

    @Size(max = 3000)
    private String codeSnippet;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Quiz quiz = (Quiz) o;
        return id.equals(quiz.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, category, question, answers, author);
    }


}
