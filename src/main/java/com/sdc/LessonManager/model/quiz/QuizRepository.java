package com.sdc.LessonManager.model.quiz;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuizRepository extends JpaRepository<Quiz,Long> {

    @Query("Select q from Quiz q WHERE q.category.id_kategoria = ?1")
    List<Quiz> findAllByCategoryId(Long categoryId);

    @Query("Select q from Quiz q WHERE q.category.kategoria = ?1")
    List<Quiz> findAllByCategoryName(String categoryName);

    List<Quiz> findByOrderByCategoryAsc();
}
