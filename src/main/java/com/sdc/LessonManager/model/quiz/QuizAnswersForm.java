package com.sdc.LessonManager.model.quiz;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class QuizAnswersForm {

    private Quiz quiz;

    private String username;

    private Boolean answerA;
    private Boolean answerB;
    private Boolean answerC;
    private Boolean answerD;

    private Boolean result;

    public QuizAnswersForm(String username){
        this.username = username;
    }

    public QuizAnswersForm(String username, Boolean answerA, Boolean answerB, Boolean answerC, Boolean answerD, Boolean result) {
        this.username = username;
        this.answerA = answerA;
        this.answerB = answerB;
        this.answerC = answerC;
        this.answerD = answerD;
        this.result = result;
    }
}
