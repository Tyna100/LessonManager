package com.sdc.LessonManager.model.quiz;


import com.sdc.LessonManager.model.entity_IndexController.Category;
import com.sdc.LessonManager.model.lesson.LessonMaterials;
import com.sdc.LessonManager.model.lesson.LessonMaterialsRepository;
import com.sdc.LessonManager.model.lesson.LessonMaterialsService;
import com.sdc.LessonManager.register_login_logout.model.User;
import com.sdc.LessonManager.register_login_logout.service.UserService;
import com.sdc.LessonManager.services.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class QuizService {
    private final QuizRepository quizRepository;
    private final UserService userService;
    private final CategoryService categoryService;
    private final AnswerService answerService;
    private final LessonMaterialsService lessonMaterialsService;

    public void addQuiz(@NotNull QuizForm quizForm){
        Quiz quiz = createQuizFromQuizForm(quizForm);
        quizRepository.save(quiz);
        saveAnswers(quizForm,quiz);
    }

    public void addQuizzes(@NotNull List<QuizForm> quizForms){
        List<Quiz> quizzes = quizForms
                .stream()
                .map(quizForm -> createQuizFromQuizForm(quizForm)).collect(Collectors.toList());
        quizRepository.saveAll(quizzes);
        for(int i = 0; i < quizForms.size(); i++){
            saveAnswers(quizForms.get(i), quizzes.get(i));
        }
    }

    private Quiz createQuizFromQuizForm(QuizForm quizForm){
        Quiz quiz = new Quiz();
        quiz.setQuestion(quizForm.getQuestion());
        quiz.setHasCodeSnippet(quizForm.getHasCodeSnippet());
        if(quizForm.getHasCodeSnippet()){
            quiz.setCodeSnippet(quizForm.getCodeSnippet());
        }
        Category category = categoryService.findCategoryById(quizForm.getCategoryId())
                .orElseThrow(()-> new IllegalArgumentException("Category with such id doesn't exist."));
        quiz.setCategory(category);

        quiz.setAuthor(quizForm.getAuthor());
        return quiz;
    }


    public void update(QuizForm quizForm, Long id){
        Quiz quizToUpdate = findById(id);
        quizToUpdate.setQuestion(quizForm.getQuestion());
        quizToUpdate.setHasCodeSnippet(quizForm.getHasCodeSnippet());
        if(quizForm.getHasCodeSnippet()){
            quizToUpdate.setCodeSnippet(quizForm.getCodeSnippet());
        }
        Category category = categoryService.findCategoryById(quizForm.getCategoryId())
                .orElseThrow(()-> new IllegalArgumentException("Category with such id doesn't exist."));
        quizToUpdate.setCategory(category);

        List<Answer> answers = quizToUpdate.getAnswers();
        answerService.update(answers.get(0).getId(),quizForm.getAnswerA(),quizForm.getIsAnswerACorrect());
        answerService.update(answers.get(1).getId(),quizForm.getAnswerB(),quizForm.getIsAnswerBCorrect());
        answerService.update(answers.get(2).getId(),quizForm.getAnswerC(),quizForm.getIsAnswerCCorrect());
        answerService.update(answers.get(3).getId(),quizForm.getAnswerD(),quizForm.getIsAnswerDCorrect());

        quizRepository.save(quizToUpdate);

    }

    public List<Quiz> findAll(){return quizRepository.findAll();}
    public List<Quiz> findAllOrderByCategory(){return quizRepository.findByOrderByCategoryAsc();}

    @Transactional
    public void deleteById(Long id, User user){
        Quiz quiz = findById(id);
        answerService.deleteByQuiz(quiz);
        List<LessonMaterials> allByUser = lessonMaterialsService.findAllByUser(user);
        allByUser.stream().forEach(lessonMaterials -> lessonMaterials.removeQuiz(quiz));
        allByUser.stream().forEach(lessonMaterials -> lessonMaterialsService.save(lessonMaterials));
        quizRepository.deleteById(id);
    }

    public List<Quiz> findAllByCategoryId(Long categoryId){return quizRepository.findAllByCategoryId(categoryId);}
    public List<Quiz> findAllByCategoryName(String categoryName){return quizRepository.findAllByCategoryName(categoryName);}

    public Quiz findById(Long id){return quizRepository.findById(id).orElseThrow(()->new IllegalArgumentException("No quiz with such id"));}

    public  void saveAnswers(QuizForm quizForm, Quiz quiz){
        Answer answerA = new Answer(quizForm.getAnswerA(), quizForm.getIsAnswerACorrect());
        answerA.setQuiz(quiz);
        answerService.save(answerA);

        Answer answerB = new Answer(quizForm.getAnswerB(),quizForm.getIsAnswerBCorrect());
        answerB.setQuiz(quiz);
        answerService.save(answerB);

        Answer answerC = new Answer(quizForm.getAnswerC(),quizForm.getIsAnswerCCorrect());
        answerC.setQuiz(quiz);
        answerService.save(answerC);

        Answer answerD = new Answer(quizForm.getAnswerD(),quizForm.getIsAnswerDCorrect());
        answerD.setQuiz(quiz);
        answerService.save(answerD);
    }

}
