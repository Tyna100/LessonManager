package com.sdc.LessonManager.model.quiz;


import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.sdc.LessonManager.active_users.ActiveUsersService;
import com.sdc.LessonManager.controllers.IndexControllers.Util.UtilClass;
import com.sdc.LessonManager.register_login_logout.model.User;
import com.sdc.LessonManager.register_login_logout.service.UserService;
import com.sdc.LessonManager.services.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
public class QuizController {
    private final QuizService quizService;
    private final SimpMessagingTemplate template;
    private final ActiveUsersService activeUsersService;
    private final CategoryService categoryService;
    private final UserService userService;

    @GetMapping("/showMaterials")
    public String showMaterials(@RequestParam(name = "type",required = true) String type,
                                @RequestParam(name = "category",required = true)String category,
                                @RequestParam(name = "favourites",required = false)boolean showFavourites,
                                RedirectAttributes redirectAttributes){

        UtilClass.saveLastSearchAttributes(Map.ofEntries(
                new AbstractMap.SimpleEntry<>("type",type),
                new AbstractMap.SimpleEntry<>("category",category),
                new AbstractMap.SimpleEntry<>("kategoria", category),
                new AbstractMap.SimpleEntry<>("favourites",String.valueOf(showFavourites))));

        redirectAttributes.addAttribute("type",type);
        redirectAttributes.addAttribute("favourites", showFavourites);
        redirectAttributes.addAttribute("category", category);
        if(type.equals("QUIZ")){
            return "redirect:/showQuizzes";
        }
        else if(type.equals("ZADANIE")){
            return "redirect:/showTasks";
        }
        else {
            return "index";
        }
    }

    @GetMapping("/addToFavourites")
    public String addToFavourites(@RequestParam(name = "type",required = true) String type,
                                  @RequestParam(name = "category",required = true)String category,
                                  @RequestParam("id") Long id,
                                  RedirectAttributes redirectAttributes){

        UtilClass.addRedirectAttributes(redirectAttributes, UtilClass.getLastSearchAttributes());

        User user = UtilClass.getLoggedInUser();


        if(type.equals("QUIZ")){
            user.getFavouriteQuizzes().add(quizService.findById(id));
            userService.updateUsersFavouriteQuizzes(user.getId(),user.getFavouriteQuizzes());
            return "redirect:/showQuizzes";
        }
        else if(type.equals("ZADANIE")){

//            todo
            return "redirect:/showTasks";
        }
        else {
            return "index";
        }
    }

    @GetMapping("/removeFromFavourites")
    public String removeFromFavourites(@RequestParam(name = "type",required = true) String type,
                                  @RequestParam(name = "category",required = true)String category,
                                  @RequestParam("id") Long id,
                                  RedirectAttributes redirectAttributes){

        UtilClass.addRedirectAttributes(redirectAttributes, UtilClass.getLastSearchAttributes());

        User user = UtilClass.getLoggedInUser();

        if(type.equals("QUIZ")){
            user.getFavouriteQuizzes().remove(quizService.findById(id));
            userService.updateUsersFavouriteQuizzes(user.getId(),user.getFavouriteQuizzes());
            return "redirect:/showQuizzes";
        }
        else if(type.equals("ZADANIE")){
            return "redirect:/showTasks";
        }
        else {
            return "index";
        }
    }

    @GetMapping("/showQuizzes")
    public String showQuizzesByCategory(@RequestParam(name = "type",required = true) String type,
                                        @RequestParam(name = "category")String category,
                                        @RequestParam(name = "favourites",required = false)boolean showFavourites,
                                        @ModelAttribute("loggedInUser") User user,Model model                                        ){


        showQuizzes(category,showFavourites,model);

        return "quiz/showQuizzesPage";
    }

    private void showQuizzes(String categoryName,boolean showFavourites, Model model){


        List<Quiz> quizzes = (categoryName.equals("ALL")) ?
                quizService.findAllOrderByCategory() : quizService.findAllByCategoryName(categoryName);

        List<Quiz> favouriteQuizzes = UtilClass.getLoggedInUser().getFavouriteQuizzes();

        if(showFavourites){
            List<Quiz> favouritesQuizzesFromCategory = quizzes.stream()
                    .filter(quiz -> favouriteQuizzes.contains(quiz))
                    .collect(Collectors.toList());
            favouritesQuizzesFromCategory.forEach(quiz -> quiz.setIsFavouriteForLoggedInUser(true));
            model.addAttribute("allQuizzes", favouritesQuizzesFromCategory);
        }
        else {
            quizzes.forEach(quiz-> quiz.setIsFavouriteForLoggedInUser(favouriteQuizzes.contains(quiz)));
            model.addAttribute("allQuizzes", quizzes);
        }

        String title;
        if(quizzes.size() == 0){
            title = "Brak quizów w tej kategorii";
        }
        else{
            title= (categoryName.equals("ALL")) ? "Quizy z wszystkich kategorii"
                    : String.format("Quizy z kategorii: %s",quizzes.get(0).getCategory().getKategoria());
        }
        model.addAttribute("categoryName", title);
    }

    @GetMapping("/addQuiz")
    public String addQuiz(Model model){

        QuizForm quizForm = new QuizForm();
        model.addAttribute("newQuiz",quizForm);

        return "quiz/addQuizPage";
    }

    @PostMapping("/addQuiz")
    public String saveQuiz(@ModelAttribute("newQuiz")@Valid QuizForm quiz, BindingResult bindingResult,
                           RedirectAttributes redirectAttributes) {

        if(bindingResult.hasErrors()){
            System.out.println("BINDING ERROR");
            return "quiz/addQuizPage";
        }
        User loggedInUser = UtilClass.getLoggedInUser();
        quiz.setAuthor(loggedInUser);
        quizService.addQuiz(quiz);

        if(categoryService.findCategoryById(quiz.getCategoryId()).isPresent()){

            UtilClass.addRedirectAttributes(redirectAttributes, UtilClass.getLastSearchAttributes());

            return "redirect:/showQuizzes";
        }
        return "index";
    }

    @GetMapping("/deleteQuiz")
    public String deleteQuizById(@RequestParam("id") Long id, RedirectAttributes redirectAttributes){
        Quiz quiz = quizService.findById(id);

        UtilClass.addRedirectAttributes(redirectAttributes, UtilClass.getLastSearchAttributes());
        User loggedInUser = UtilClass.getLoggedInUser();
        if(!loggedInUser.equals(quiz.getAuthor())){
            return "redirect:/showQuizzes";
        }
        if( loggedInUser.getActiveLessonMaterials() != null) {
            loggedInUser.getActiveLessonMaterials().getQuizzes().remove(quiz);
        }
        loggedInUser.getFavouriteQuizzes().remove(quiz);
        userService.updateUsersFavouriteQuizzes(loggedInUser.getId(),loggedInUser.getFavouriteQuizzes());
        quizService.deleteById(id, loggedInUser);
        return "redirect:/showQuizzes";
    }

    @GetMapping("/editQuiz")
    public String showEditQuiz(@RequestParam(name = "id")Long id, Model model){
        QuizForm quizForm = new QuizForm();
        Quiz quizById = quizService.findById(id);

        List<Answer>answers = quizById.getAnswers();
        quizForm.setAnswerA(answers.get(0).getAnswer());
        quizForm.setIsAnswerACorrect(answers.get(0).getIsCorrectAnswer());
        quizForm.setAnswerB(answers.get(1).getAnswer());
        quizForm.setIsAnswerBCorrect(answers.get(1).getIsCorrectAnswer());
        quizForm.setAnswerC(answers.get(2).getAnswer());
        quizForm.setIsAnswerCCorrect(answers.get(2).getIsCorrectAnswer());
        quizForm.setAnswerD(answers.get(3).getAnswer());
        quizForm.setIsAnswerDCorrect(answers.get(3).getIsCorrectAnswer());

        quizForm.setQuestion(quizById.getQuestion());
        quizForm.setCategoryId(quizById.getCategory().getId_kategoria());

        model.addAttribute("newQuiz",quizForm);
        return "quiz/editQuizPage";
    }

    @PostMapping("/editQuiz")
    public String editQuiz(@RequestParam("id")Long id, @ModelAttribute("newQuiz")@Valid QuizForm quizForm,
                           BindingResult bindingResult, RedirectAttributes redirectAttributes){

               if(bindingResult.hasErrors()){
            return "quiz/editQuizPage";
        }
        Quiz quiz = quizService.findById(id);

        quizService.update(quizForm,id);

        if(UtilClass.getLoggedInUser().getActiveLessonMaterials() != null){
            int index = UtilClass.getLoggedInUser().getActiveLessonMaterials().getQuizzes().indexOf(quiz);
            UtilClass.getLoggedInUser().getActiveLessonMaterials().getQuizzes().set(index,quizService.findById(id));
        }

        UtilClass.addRedirectAttributes(redirectAttributes, UtilClass.getLastSearchAttributes());

        return "redirect:/showQuizzes";
    }

    @GetMapping("/shareQuiz")
    public String shareQuiz(@RequestParam(name = "id")Long id, Model model){
        String destination = String.format("/topic/quiz/%s",UtilClass.getLoggedInUser().getActiveGroupId());
        template.convertAndSend(destination,id);
        List<QuizAnswersForm> quizResultsAllStudents = new ArrayList<>();
        Long groupId = UtilClass.getLoggedInUser().getActiveGroupId();
        List<User> activeUsersByGroupId = activeUsersService.getActiveStudentsByGroupId(groupId);
        activeUsersByGroupId.forEach(user -> quizResultsAllStudents.add(new QuizAnswersForm(user.getUsername())));
        activeUsersService.initQuizForGroup(id,groupId);

        model.addAttribute("quizResultsAllStudents",quizResultsAllStudents);
        model.addAttribute("quiz",quizService.findById(id));
        return "quiz/showQuizResultsAllStudentsPage";
    }

    @GetMapping("/endQuiz")
    public String endQuiz(@RequestParam(name = "id")Long id){
        String destination = String.format("/topic/quiz/%s",UtilClass.getLoggedInUser().getActiveGroupId());
        template.convertAndSend(destination,-1);

        String groupQuizName = UtilClass.getLoggedInUser().getActiveGroupId() + " " + id;
        activeUsersService.removeQuiz(groupQuizName);
        return "index";
    }

    @GetMapping("/showQuiz")
    public String showQuiz(@ModelAttribute("quizAnswersForm")QuizAnswersForm quizAnswersForm,
                           @RequestParam(name = "id")Long id, Model model){
        Quiz quiz = quizService.findById(id);
        quizAnswersForm.setQuiz(quiz);
        model.addAttribute("quizAnswersForm", quizAnswersForm);

        return "quiz/showQuizPage";
    }

    @PostMapping("/checkQuiz")
    public String checkQuiz(@ModelAttribute("quizAnswersForm")QuizAnswersForm quizAnswersForm,
                            @RequestParam(name = "id")Long id, Model model,
                            RedirectAttributes redirectAttributes){
        Quiz quiz = quizService.findById(id);
        Boolean[] results = new Boolean[4];

        results[0] =  quizAnswersForm.getAnswerA()==quiz.getAnswers().get(0).getIsCorrectAnswer();
        results[1] =  quizAnswersForm.getAnswerB()==quiz.getAnswers().get(1).getIsCorrectAnswer();
        results[2] =  quizAnswersForm.getAnswerC()==quiz.getAnswers().get(2).getIsCorrectAnswer();
        results[3] =  quizAnswersForm.getAnswerD()==quiz.getAnswers().get(3).getIsCorrectAnswer();

        String groupQuizName = UtilClass.getLoggedInUser().getActiveGroupId() + " " + id;
        activeUsersService.updateQuizForGroup(groupQuizName,
                new QuizAnswersForm(UtilClass.getLoggedInUser().getUsername(),results[0],results[1],results[2],results[3],
                        results[0]&&results[1]&&results[2]&&results[3]));

        redirectAttributes.addAttribute("a",results[0]);
        redirectAttributes.addAttribute("b",results[1]);
        redirectAttributes.addAttribute("c",results[2]);
        redirectAttributes.addAttribute("d",results[3]);

        model.addAttribute("results",results);
        model.addAttribute("quiz",quiz);

        return "quiz/showQuizResultsPage";
    }

    @PostMapping(value = "/uploadCsvFile", consumes = {"multipart/form-data"})
    public String createMultipleQuizzesFromCsv(@RequestParam MultipartFile file, RedirectAttributes redirectAttributes){

        try(Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))){
            CsvToBean<QuizForm> csvToBean = new CsvToBeanBuilder<QuizForm>(reader)
                    .withType(QuizForm.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            List<QuizForm> forms = csvToBean.parse();
            User loggedInUser = UtilClass.getLoggedInUser();
            forms.forEach(quizForm -> quizForm.setAuthor(loggedInUser));
            quizService.addQuizzes(forms);

            redirectAttributes.addAttribute("type","QUIZ");
            redirectAttributes.addAttribute("category", "ALL");
            redirectAttributes.addAttribute("favourite", "false");

        }catch (IOException e){
            e.printStackTrace();
        }
        return "redirect:/showQuizzes";
    }

}
