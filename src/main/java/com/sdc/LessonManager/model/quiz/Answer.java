package com.sdc.LessonManager.model.quiz;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Answer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String answer;

    private Boolean isCorrectAnswer;

    @ManyToOne
    private Quiz quiz;

    public Answer(String answer, Boolean isCorrectAnswer){
        this.answer = answer;
        this.isCorrectAnswer = isCorrectAnswer;
    }

}
