package com.sdc.LessonManager.model.quiz;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AnswerRepository extends JpaRepository<Answer,Long> {
    void deleteByQuiz(Quiz quiz);
}
