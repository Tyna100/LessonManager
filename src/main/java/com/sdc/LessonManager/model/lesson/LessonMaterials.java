package com.sdc.LessonManager.model.lesson;


import com.sdc.LessonManager.model.entity_IndexController.Task;
import com.sdc.LessonManager.model.quiz.Quiz;
import com.sdc.LessonManager.register_login_logout.model.User;
import lombok.Data;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Entity
public class LessonMaterials {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Pole nie może być puste")
    private String name;

    @OneToMany (mappedBy = "lessonMaterials")
    private List<Lesson> lessons;

    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Quiz> quizzes;

    @Transient
    List<Quiz> usedQuizzes;


    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Task> tasks;

    @Transient
    List<Task> usedTasks;

    @ManyToOne
    private User user;

    @Transient
    List<String> categoryNames;

    public LessonMaterials() {
        quizzes = new ArrayList<>();
        tasks = new ArrayList<>();
        usedQuizzes = new ArrayList<>();
        usedTasks = new ArrayList<>();
    }

    public boolean removeQuiz(Quiz quiz){
       return quizzes.remove(quiz);
    }
    public boolean removeTask(Task task){return  tasks.remove(task);}
}
