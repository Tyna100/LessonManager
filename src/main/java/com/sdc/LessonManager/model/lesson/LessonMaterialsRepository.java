package com.sdc.LessonManager.model.lesson;

import com.sdc.LessonManager.register_login_logout.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LessonMaterialsRepository extends JpaRepository<LessonMaterials, Long> {
    List<LessonMaterials> findAllByUser(User user);
}
