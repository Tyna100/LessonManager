package com.sdc.LessonManager.model.lesson;

import com.sdc.LessonManager.register_login_logout.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class LessonMaterialsService {
    private final LessonMaterialsRepository lessonMaterialsRepository;

    public List<LessonMaterials> findAllByUser(User user){
        return lessonMaterialsRepository.findAllByUser(user);
    }

    public void save(LessonMaterials lessonMaterials){
        lessonMaterialsRepository.save(lessonMaterials);
    }

    public void remove(Long id){
        lessonMaterialsRepository.deleteById(id);}

    public LessonMaterials findById(Long id){
        return lessonMaterialsRepository.findById(id).orElseThrow(()->new IllegalArgumentException("LessonsMaterials with such id doesn't exist"));
    }

    public void update(Long id, LessonMaterials lessonMaterials){
        LessonMaterials toUpdate = findById(id);

        toUpdate.setName(lessonMaterials.getName());
        toUpdate.setQuizzes(lessonMaterials.getQuizzes());
        toUpdate.setTasks(lessonMaterials.getTasks());

        lessonMaterialsRepository.save(toUpdate);
    }



}
