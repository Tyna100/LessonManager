package com.sdc.LessonManager.model.lesson;

import com.sdc.LessonManager.active_users.ActiveUsersService;
import com.sdc.LessonManager.controllers.IndexControllers.Util.LastSearchAttributes;
import com.sdc.LessonManager.controllers.IndexControllers.Util.UtilClass;
import com.sdc.LessonManager.register_login_logout.model.User;
import com.sdc.LessonManager.services.GroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;
import java.util.Map;

@Controller
@RequiredArgsConstructor
public class LessonController {
    private final GroupService groupService;
    private final ActiveUsersService activeUsersService;

    @GetMapping("/createLesson")
    public String createLesson(RedirectAttributes redirectAttributes, Model model){
        User user = UtilClass.getLoggedInUser();
        Lesson lesson = new Lesson();
        lesson.setDate(LocalDate.now());
        lesson.setTeacher(user);
        lesson.setGroup(groupService.findById(user.getActiveGroupId()));
        lesson.setLessonMaterials(new LessonMaterials());
        lesson.setStudents(activeUsersService.getActiveStudentsByGroupId(user.getActiveGroupId()));

        UtilClass.addRedirectAttributes(redirectAttributes, UtilClass.getLastSearchAttributes());
        return "redirect:/showMaterials";
    }
}
