package com.sdc.LessonManager.model.lesson;

import com.sdc.LessonManager.controllers.IndexControllers.Util.UtilClass;
import com.sdc.LessonManager.model.entity_IndexController.Task;
import com.sdc.LessonManager.model.quiz.Quiz;
import com.sdc.LessonManager.model.quiz.QuizService;
import com.sdc.LessonManager.register_login_logout.model.User;
import com.sdc.LessonManager.services.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@RequiredArgsConstructor
public class LessonMaterialsController {
    private final QuizService quizService;
    private final TaskService taskService;
    private final LessonMaterialsService lessonMaterialsService;

    @GetMapping("/showLessonMaterials")
    public String showLessons(@RequestParam(name = "type")String type, Model model){
        User loggedInUser = UtilClass.getLoggedInUser();
        List<LessonMaterials> allByUser = lessonMaterialsService.findAllByUser(loggedInUser);

        List<List<String>> categoryNamesList = allByUser.stream()
                .map(lessonMaterials -> Stream.of(
                lessonMaterials.getQuizzes().stream().map(q-> q.getCategory().getKategoria()).collect(Collectors.toList()),
                lessonMaterials.getTasks().stream().map(t-> t.getKategoria()).collect(Collectors.toList())).flatMap(List::stream).distinct().collect(Collectors.toList()))
                .collect(Collectors.toList());

        for(int i = 0; i<allByUser.size(); i++){
            allByUser.get(i).setCategoryNames(categoryNamesList.get(i));
        }

        model.addAttribute("allLessonMaterials", allByUser);
        model.addAttribute("newLessonPack", new LessonMaterials());
        return "lesson/showLessonMaterialsPage";
    }

    @PostMapping("/prepareLessonMaterials")
    public String startPreparingLessonMaterials(@Valid @ModelAttribute(name = "newLessonPack") LessonMaterials lessonMaterials,
                                                BindingResult bindingResult, RedirectAttributes redirectAttributes){
        if(bindingResult.hasErrors()){
            return "lesson/showLessonMaterialsPage";
        }
        return activateLessonMaterials(lessonMaterials,redirectAttributes);
    }

    @GetMapping("/editLessonMaterials")
    public String editLessonMaterials(@RequestParam(name = "id") Long id, RedirectAttributes redirectAttributes){
        return activateLessonMaterials(lessonMaterialsService.findById(id),redirectAttributes);
    }

    private String activateLessonMaterials(LessonMaterials lessonMaterials, RedirectAttributes redirectAttributes){
        UtilClass.getLoggedInUser().setIsPreparingLessonMaterials(true);
        UtilClass.getLoggedInUser().setActiveLessonMaterials(lessonMaterials);
        redirectAttributes.addAttribute("type","QUIZ");
        redirectAttributes.addAttribute("category","ALL");
        redirectAttributes.addAttribute("favourites","false");
        return "redirect:/showQuizzes";
    }

//    @GetMapping("/addToUsed")
//    public String addToUsed(@RequestParam(name = "type")String type, @RequestParam(name = "id")Long id, RedirectAttributes redirectAttributes){
//        LessonMaterials activeLessonMaterials = UtilClass.getLoggedInUser().getActiveLessonMaterials();
//    }

    @GetMapping("/addToLesson")
    public String addToLesson(@RequestParam(name = "type")String type, @RequestParam(name = "id")Long id, RedirectAttributes redirectAttributes){
        LessonMaterials activeLessonMaterials = UtilClass.getLoggedInUser().getActiveLessonMaterials();
        switch (type){
            case "QUIZ":
                activeLessonMaterials.getQuizzes().add(quizService.findById(id));
                break;
            case "ZADANIE":
                Optional<Task> oTask = taskService.findById(id);
                oTask.ifPresent(task -> activeLessonMaterials.getTasks().add(task));
                break;
        }
        redirectAttributes.addAttribute("type",type);
        redirectAttributes.addAttribute("category","ALL");
        return "redirect:/showMaterials";
    }

    @GetMapping("/removeFromLesson")
    public String removeFromLesson(@RequestParam(name = "type")String type, @RequestParam(name = "id")Long id, RedirectAttributes redirectAttributes){
        LessonMaterials activeLessonMaterials = UtilClass.getLoggedInUser().getActiveLessonMaterials();

        switch (type){
            case "QUIZ":
                Quiz quiz = quizService.findById(id);
                boolean remove = activeLessonMaterials.removeQuiz(quiz);
                break;
            case "ZADANIE":
                Optional<Task> oTask = taskService.findById(id);
                oTask.ifPresent(task -> activeLessonMaterials.getTasks().remove(task));
                break;
        }
        redirectAttributes.addAttribute("type",type);
        redirectAttributes.addAttribute("category","ALL");
        return "redirect:/showMaterials";
    }

    @GetMapping("/saveLessonMaterials")
    public String saveLessonMaterials(RedirectAttributes redirectAttributes){
        User loggedInUser = UtilClass.getLoggedInUser();
//        loggedInUser.setIsPreparingLessonMaterials(false);

        LessonMaterials activeLessonMaterials = loggedInUser.getActiveLessonMaterials();

        if(activeLessonMaterials.getUser() == null){
            activeLessonMaterials.setUser(loggedInUser);
            lessonMaterialsService.save(activeLessonMaterials);}
        else {
            lessonMaterialsService.update(activeLessonMaterials.getId(), activeLessonMaterials);
        }
        redirectAttributes.addAttribute("type","personal");
//        loggedInUser.setActiveLessonMaterials(null);
        return "redirect:/showLessonMaterials";
    }

    @GetMapping("/closeLessonMaterials")
    public String closeLessonMaterials(@RequestParam(name = "save")Boolean save, RedirectAttributes redirectAttributes){
        User loggedInUser = UtilClass.getLoggedInUser();
        loggedInUser.setIsPreparingLessonMaterials(false);

        if(save){
            LessonMaterials activeLessonMaterials = loggedInUser.getActiveLessonMaterials();
            if(activeLessonMaterials.getUser() == null){
                activeLessonMaterials.setUser(loggedInUser);
                lessonMaterialsService.save(activeLessonMaterials);}
            else {
                lessonMaterialsService.update(activeLessonMaterials.getId(), activeLessonMaterials);
            }
        }

        redirectAttributes.addAttribute("type","personal");
        loggedInUser.setActiveLessonMaterials(null);
        return "redirect:/showLessonMaterials";
    }

    @GetMapping("/deleteLessonMaterials")
    public String deleteLessonMaterials(@RequestParam(name = "id")Long id, RedirectAttributes redirectAttributes){
        redirectAttributes.addAttribute("type","personal");
        System.out.println(id);
        lessonMaterialsService.remove(id);
        return "redirect:/showLessonMaterials";
    }

}
