package com.sdc.LessonManager.DB_populate_service;

import com.sdc.LessonManager.active_users.ActiveUsersService;
import com.sdc.LessonManager.model.entity_IndexController.Category;
import com.sdc.LessonManager.model.entity_IndexController.Grupa;
import com.sdc.LessonManager.model.entity_IndexController.repo.CategoryRepo;
import com.sdc.LessonManager.model.entity_IndexController.repo.GrupaRepo;
import com.sdc.LessonManager.model.lesson.LessonMaterials;
import com.sdc.LessonManager.model.lesson.LessonMaterialsRepository;
import com.sdc.LessonManager.model.quiz.Answer;
import com.sdc.LessonManager.model.quiz.AnswerRepository;
import com.sdc.LessonManager.model.quiz.Quiz;
import com.sdc.LessonManager.model.quiz.QuizRepository;
import com.sdc.LessonManager.register_login_logout.model.CourseRole;
import com.sdc.LessonManager.register_login_logout.model.Role;
import com.sdc.LessonManager.register_login_logout.model.User;
import com.sdc.LessonManager.register_login_logout.model.repository.RoleRepository;
import com.sdc.LessonManager.register_login_logout.model.repository.UserRepository;
import com.sdc.LessonManager.register_login_logout.service.UserService;
import com.sdc.LessonManager.services.GroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

@Component
@RequiredArgsConstructor
public class DBPopulatingService implements ApplicationListener<ApplicationReadyEvent> {
private final UserRepository userRepository;
private final UserService userService;
private final GrupaRepo grupaRepo;
private final RoleRepository roleRepository;
private final GroupService groupService;
private final CategoryRepo categoryRepo;
private final AnswerRepository answerRepository;
private final QuizRepository quizRepository;
private final LessonMaterialsRepository lessonMaterialsRepository;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {

        addRoles();
        addGroups();
        addUsers();
        addCategories();
        addQuizzes();
        addLessonMaterials();

        ActiveUsersService.map = new HashMap<>();
        Iterable<Grupa> groups = groupService.findAll();
        groups.forEach(group -> ActiveUsersService.map.put(group.getId_grupa(), new ArrayList<>()));

    }

    private void addRoles(){
        userService.addRoles("USER","ADMIN");
    }

    private  void addGroups(){
        Grupa grupa1 = new Grupa();
        grupa1.setName("JAVA_GDA30");
        grupaRepo.save(grupa1);

        Grupa grupa2 = new Grupa();
        grupa2.setName("JAVA_GDA29");
        grupaRepo.save(grupa2);

        Grupa grupa3 = new Grupa();
        grupa3.setName("PYTHON_GDA29");
        grupaRepo.save(grupa3);
    }

    private void addUsers(){
        HashSet<Role> roles = new HashSet<>();
        roles.add(roleRepository.findByName("USER"));

        User user = new User();
        user.setUsername("teacher@gmail.com");
        user.setPassword("password");
        user.setPasswordConfirm("password");
        user.setCourseRole(CourseRole.TEACHER);
        user.setRoles(new HashSet<>(roles));
        userService.save(user);

        User user2 = new User();
        user2.setUsername("student@gmail.com");
        user2.setPassword("password");
        user2.setPasswordConfirm("password");
        user2.setCourseRole(CourseRole.STUDENT);
        user2.setRoles(new HashSet<>(roles));
        userService.save(user2);

        User u  = userRepository.findByUsername("teacher@gmail.com");
        u.setGroups(grupaRepo.findAll());
        userRepository.save(u);

        User u2  = userRepository.findByUsername("student@gmail.com");
        u2.setGroups(grupaRepo.findAll());
        userRepository.save(u2);
    }

    private void addCategories(){
        Category category1 = new Category("STRINGS");
        categoryRepo.save(category1);
        Category category2 = new Category("REGEX");
        categoryRepo.save(category2);
        Category category3 = new Category("DZIEDZICZENIE");
        categoryRepo.save(category3);
        Category category4 = new Category("VARIABLES");
        categoryRepo.save(category4);
    }

    private void addQuizzes(){
        Quiz quiz = new Quiz();

        quiz.setQuestion("test question 1");
        Category category = categoryRepo.findById(1L)
                .orElseThrow(()-> new IllegalArgumentException("Category with such id doesn't exist."));
        quiz.setCategory(category);

        quiz.setAuthor(userRepository.findByUsername("teacher@gmail.com"));
        quiz.setHasCodeSnippet(false);

        quizRepository.save(quiz);

        Answer a = new Answer("A ok", true);
        a.setQuiz(quiz);
        answerRepository.save(a);
        Answer b = new Answer("B nope", false);
        b.setQuiz(quiz);
        answerRepository.save(b);
        Answer c = new Answer("C nope", false);
        c.setQuiz(quiz);
        answerRepository.save(c);
        Answer d = new Answer("D ok", true);
        d.setQuiz(quiz);
        answerRepository.save(d);

        Quiz quiz_2 = new Quiz();

        quiz_2.setQuestion("test question 2");
        Category category2 = categoryRepo.findById(2L)
                .orElseThrow(()-> new IllegalArgumentException("Category with such id doesn't exist."));
        quiz_2.setCategory(category2);

        quiz_2.setAuthor(userRepository.findByUsername("teacher@gmail.com"));
        quiz_2.setHasCodeSnippet(false);

        quizRepository.save(quiz_2);

        Answer a2 = new Answer("A nope", false);
        a2.setQuiz(quiz_2);
        answerRepository.save(a2);
        Answer b2 = new Answer("B nope", false);
        b2.setQuiz(quiz_2);
        answerRepository.save(b2);
        Answer c2 = new Answer("C nope", false);
        c2.setQuiz(quiz_2);
        answerRepository.save(c2);
        Answer d2 = new Answer("D ok", true);
        d2.setQuiz(quiz_2);
        answerRepository.save(d2);

    }

    private void addLessonMaterials(){
        LessonMaterials lessonMaterials = new LessonMaterials();
        lessonMaterials.setName("NowaLekcja");
        lessonMaterials.setUser(userRepository.findByUsername("teacher@gmail.com"));
        lessonMaterialsRepository.save(lessonMaterials);
    }

}
