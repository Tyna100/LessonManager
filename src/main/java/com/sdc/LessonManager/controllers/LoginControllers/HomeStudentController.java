package com.sdc.LessonManager.controllers.LoginControllers;

import com.sdc.LessonManager.register_login_logout.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class HomeStudentController {

    @RequestMapping(value = "/home-s")
    public String showStudentHomePage(HttpServletRequest request, Model model){
        HttpSession session = request.getSession(false);
        User user = (User) session.getAttribute("appUser");

        model.addAttribute("userName", user.getUsername());
        model.addAttribute("sessionId", session.getId());


        return "home-s";
    }
}
