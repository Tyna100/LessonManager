package com.sdc.LessonManager.controllers.IndexControllers;


import com.sdc.LessonManager.controllers.IndexControllers.Util.UtilClass;
import com.sdc.LessonManager.model.entity_IndexController.Task;
import com.sdc.LessonManager.services.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

@RequiredArgsConstructor
@Controller
public class AddTaskController {
    private final TaskService taskService;
    private final IndexController indexController;

    @GetMapping("/addtask")
    public String getCategory(Model model, HttpServletRequest request) {
        return "task/addTaskPage";
    }

    @RequestMapping(
            value = "/addtask",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)

    public String addtask(@ModelAttribute("noweZadanie") @RequestBody Task task, Model model, final RedirectAttributes redirectAttributes) {
        String _ERROR = "";
        String _OK = null;
        String return_text = "redirect:/addtask";
        String trescX = task.getTresc_zadania();
        String rozwiazanie = task.getRozwiazanie_zadania();
        String wskazowka = task.getWskazowka();
        task.setId_trenera(UtilClass.getLoggedInUser().getUsername());

        if (trescX == null || trescX.isEmpty() || trescX.equals("") || trescX.trim().isEmpty() ) {
            _ERROR += "Treść zadania nie może być pusta" + System.lineSeparator();
            trescX = "";
        }
        if (rozwiazanie == null || rozwiazanie.isEmpty() || rozwiazanie.equals("") || rozwiazanie.trim().isEmpty() ) {
            _ERROR += "Rozwiązanie zadania nie może być puste" + System.lineSeparator();
            rozwiazanie = "";
        }
        if (wskazowka == null || wskazowka.isEmpty() || wskazowka.equals("") || wskazowka.trim().isEmpty() ) {
            _ERROR += "Wskazówka zadania nie może być puste" + System.lineSeparator();
            wskazowka = "";
        }
         else {
            _OK = "Dodałeś zadanie ";
            taskService.addTask(task);
            _ERROR = null;
        }
        redirectAttributes.addFlashAttribute("trescX", trescX);
        redirectAttributes.addFlashAttribute("rozwiazanie", rozwiazanie);
        redirectAttributes.addFlashAttribute("wskazowka", wskazowka);
        redirectAttributes.addFlashAttribute("_ERROR", _ERROR);
        redirectAttributes.addFlashAttribute("_OK", _OK);
        if (_ERROR==null) {
            return_text = "redirect:/showMaterials?type=ZADANIE&category=" + task.getKategoria();
        }
        return return_text;
    }

}
