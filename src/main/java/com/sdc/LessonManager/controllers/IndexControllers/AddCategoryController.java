package com.sdc.LessonManager.controllers.IndexControllers;

import com.sdc.LessonManager.model.entity_IndexController.Category;
import com.sdc.LessonManager.services.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.Set;

@RequiredArgsConstructor
@Controller
public class AddCategoryController {

    private final CategoryService categoryService;


    @GetMapping("/addcategory")
    public String getCategory(Model model, HttpServletRequest request) {
        model.addAttribute("nowa_Kategoria", new Category());
        return "task/addCategoryPage";
    }

    @PostMapping("/addcategory")
    public String addCategory(@ModelAttribute("nowa_Kategoria") @RequestBody Category category, Model model, BindingResult bindingResult, final RedirectAttributes redirectAttributes) {

        String _ERROR = null;
        String _OK = null;
        String value = null;
        String return_text="redirect:/addcategory"  ;
        Set<String> allcategory = makeCollection(categoryService.getAllcategory());
        String kategoria = category.getKategoria().trim();
        if (category.getKategoria() == null || category.getKategoria().isEmpty() || category.getKategoria().equals("") ||kategoria.isEmpty()|| bindingResult.hasErrors()) {
            _ERROR = "Spróbuj jeszcze raz";

        } else if (allcategory.contains(category.getKategoria())) {
            _ERROR = String.format("Kategoria '%s' istnieje",category.getKategoria());
            value = category.getKategoria();

        } else {
            _OK = "Dodałeś kategorię : " + category.getKategoria();
            categoryService.addcategory(category);
            return_text="redirect:/index";
        }
        redirectAttributes.addFlashAttribute("value", value);
        redirectAttributes.addFlashAttribute("_ERROR", _ERROR);
        redirectAttributes.addFlashAttribute("_OK", _OK);
        return return_text;
    }

    public static HashSet<String> makeCollection(Iterable<Category> iter) {
        HashSet<String> hashset = new HashSet<>();
        for (Category item : iter) {
            hashset.add(item.getKategoria());
        }
        return hashset;
    }
}
