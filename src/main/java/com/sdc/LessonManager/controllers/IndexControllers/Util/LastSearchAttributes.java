package com.sdc.LessonManager.controllers.IndexControllers.Util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LastSearchAttributes {
    private Map<String,String> attributes = new HashMap<>();

}
