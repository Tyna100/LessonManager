package com.sdc.LessonManager.controllers.IndexControllers;

import com.sdc.LessonManager.register_login_logout.model.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RequiredArgsConstructor
@Controller
public class LeftPaneController {
    private final UserRepository userRepository;

    @GetMapping("/leftpane")
    private String getLeftPane(Model model) {
        return "leftpane";
    }

    @PostMapping("/showListOfUser")
    public String getAllLogins(@ModelAttribute("allLogins") @RequestBody Model model) {
        return "redirect:/index";
    }
}
