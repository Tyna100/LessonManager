package com.sdc.LessonManager.controllers.IndexControllers;


import com.sdc.LessonManager.active_users.ActiveUsersPackage;
import com.sdc.LessonManager.active_users.ActiveUsersService;
import com.sdc.LessonManager.controllers.IndexControllers.Util.LastSearchAttributes;
import com.sdc.LessonManager.controllers.IndexControllers.Util.UtilClass;
import com.sdc.LessonManager.model.entity_IndexController.*;
import com.sdc.LessonManager.model.entity_loginController.Login_Password;
import com.sdc.LessonManager.model.entity_loginController.repo.Login_PasswordRepo;
import com.sdc.LessonManager.model.lesson.LessonMaterials;
import com.sdc.LessonManager.model.quiz.Quiz;
import com.sdc.LessonManager.model.quiz.QuizAnswersForm;
import com.sdc.LessonManager.register_login_logout.model.User;
import com.sdc.LessonManager.services.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.print.attribute.HashAttributeSet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;

@Data
@RequiredArgsConstructor
@ControllerAdvice
@Controller
public class IndexController {
    private Login_PasswordRepo login_passwordRepo;
    private final TaskService taskService;
    private final CategoryService categoryService;
    private final GroupService groupService;
    private final MessengerService messengerService;
    private final KursService kursService;
    private final ActiveUsersService activeUsersService;
    private final UserTestService userTestService;
    private String Login;

    @GetMapping(value = {"/index", "/home-t", "/home-s"})
    public String get(RedirectAttributes redirectAttributes) {
        redirectAttributes.addAttribute("type", "QUIZ");
        redirectAttributes.addAttribute("favourites",false);
        redirectAttributes.addAttribute("category", "ALL");

        System.out.print("ActiveUsers: ");
        ActiveUsersService.map.get(UtilClass.getLoggedInUser().getActiveGroupId()).forEach(user -> System.out.print(user.getUsername() + ", "));
        System.out.println("\n");

        return "redirect:/showMaterials";
    }

    @ModelAttribute
    public void AddAttributes(Model model, HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        User appUser = null;
        if (session != null) appUser = (User) session.getAttribute("appUser");
        if (appUser != null) {
            ServletPath servletPath = UtilClass.getServletPath();
            User user = UtilClass.getLoggedInUser();

            Long activeGroupId = user.getActiveGroupId();
            ActiveUsersPackage activeUsers = activeUsersService.getActiveUsersPackage(activeGroupId);
            Login = user.getUsername();

            //Logowanie
            model.addAttribute("allLogins", activeUsers);
            model.addAttribute("loggedInUser", user);
            model.addAttribute("contextpath", servletPath);
            model.addAttribute("nowyLoginOkno", new Login_Password());
            //Zadania
//            model.addAttribute("href", userTestService.loadHref(activeGroupId));
            model.addAttribute("Tasks", taskService.getAll());
            model.addAttribute("noweZadanie", new Task());
            model.addAttribute("wybierz_zadanie", new Task());
            //Kategorie
            model.addAttribute("nowaKategoria", new Category());
            model.addAttribute("listOfCategory", categoryService.getAllcategory());
            //Messenger
            model.addAttribute("komunikator", new Messenger());
            model.addAttribute("wyswtekst", messengerService.findAllMessagesById(activeGroupId));
            //Grupy
            model.addAttribute("nowaGrupa", new Grupa());
            model.addAttribute("listOfGrupa", user.getGroups());
            //Quiz
            model.addAttribute("chosenCategoryIdQuizzes", new Quiz());
            model.addAttribute("newQuizPath","/showQuiz/");
            model.addAttribute("QuizAnswersForm",new QuizAnswersForm());

            model.addAttribute("newLessonContent",new LessonMaterials());

        }
    }
}

