package com.sdc.LessonManager.controllers.IndexControllers.Util;

import com.sdc.LessonManager.model.entity_IndexController.ServletPath;
import com.sdc.LessonManager.register_login_logout.model.User;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class UtilClass {

    public static User getLoggedInUser(){
        HttpSession session = getCurrentHttpRequest().getSession(false);
        return (User)session.getAttribute("appUser");
    }
    public static HttpServletRequest getCurrentHttpRequest(){
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes instanceof ServletRequestAttributes) {
            HttpServletRequest request = ((ServletRequestAttributes)requestAttributes).getRequest();
            return request;
        }
        return null;
    }

    public static ServletPath getServletPath() {
        ServletPath servletPath = new ServletPath();
        servletPath.setPath(getCurrentHttpRequest().getContextPath());
        return servletPath;
    }

    public static void updateLoggedInUser(User updatedUser){
        HttpSession session = getCurrentHttpRequest().getSession(false);
        session.setAttribute("appUser",updatedUser);
    }

    public static void saveLastSearchAttributes(Map<String,String> attributes){
        HttpSession session = getCurrentHttpRequest().getSession(false);
            StringBuilder sb = new StringBuilder();
            attributes.forEach((name,value)->{
                sb.append(name + " ");
                session.setAttribute(name,value);});

            session.setAttribute("lastAttributesNames",sb.toString().trim());
    }

    public static Map<String,String> getLastSearchAttributes(){
        HttpSession session = getCurrentHttpRequest().getSession(false);
        Map<String,String> lastAttributes = new HashMap<>();
            String lastAttributesNamesString = (String) session.getAttribute("lastAttributesNames");
            String[] lastAttributesNames = lastAttributesNamesString.split(" ");
            Arrays.asList(lastAttributesNames).forEach(name -> lastAttributes.put(name,(String)session.getAttribute(name)));

        return lastAttributes;
    }

    public static void addRedirectAttributes(RedirectAttributes redirectAttributes, Map<String,String> attributes){
        attributes.forEach((name,value)-> {
            redirectAttributes.addAttribute(name, value);
            System.out.println(name + " " + value);
        });
    }

}
