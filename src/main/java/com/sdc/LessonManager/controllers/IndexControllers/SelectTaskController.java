package com.sdc.LessonManager.controllers.IndexControllers;


import com.sdc.LessonManager.controllers.IndexControllers.Util.UtilClass;
import com.sdc.LessonManager.model.entity_IndexController.Task;
import com.sdc.LessonManager.model.entity_IndexController.repo.TaskRepo;
import com.sdc.LessonManager.services.CategoryService;
import com.sdc.LessonManager.services.TaskService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;

@Data
@Controller
@RequiredArgsConstructor
public class SelectTaskController {
    private final TaskService taskService;
    private final TaskRepo taskRepo;
    private final CategoryService categoryService;

    @GetMapping("/showTasks")
    private String showTests(Model model, @RequestParam(name = "type",required = true) String type,
                             @RequestParam(name = "category",required = true) String kategoria) throws AuthenticationException {
        Task task=new Task();
        task.setKategoria(kategoria);
        model.addAttribute("wybierz_zadanie",task);
        model.addAttribute("listOfPreSelectedTasks",taskService.getListOfSelectedTask(kategoria, UtilClass.getLoggedInUser().getActiveGroupId()));
        return "task/showTasksPage";}


    @GetMapping("/showtest")
    public String showSpecificTest(@RequestParam("id")Long id, Model model, HttpServletRequest request) throws AuthenticationException {
        Task task= taskRepo.findById(id).orElseThrow(()->new IllegalArgumentException("Nie ma takiego zadania"));
        model.addAttribute("singleTask",task);
        return "task/showSingleTaskPage";
    }

    @GetMapping("/verifytest")
    public String verifySpecificTest(@RequestParam("id")Long id, Model model, HttpServletRequest request) throws AuthenticationException {
        Task task= taskRepo.findById(id).orElseThrow(()->new IllegalArgumentException("Nie ma takiego zadania"));
        model.addAttribute("singleTask",task);
        return "task/verifySingleTaskPage";
    }



    @GetMapping("/edittest")
    public String editSpecificTest(@RequestParam("id")Long id, Model model, HttpServletRequest request) throws AuthenticationException {
        Task task= taskRepo.findById(id).orElseThrow(()->new IllegalArgumentException("Nie ma takiego zadania"));
        model.addAttribute("singleTask",task);
        return "task/editSingleTaskPage";
    }

    @PostMapping("/edittest")
    public String editwSpecificTest(@ModelAttribute("singleTask") Task task, @RequestParam("id")Long id, HttpServletRequest request, RedirectAttributes redirectAttributes) throws AuthenticationException {
        String _ERROR = "";
        String _OK = null;
        String return_text = "redirect:/editTask"+id;
        String trescX = task.getTresc_zadania();
        String rozwiazanie = task.getRozwiazanie_zadania();
        String wskazowka = task.getWskazowka();

        if (trescX == null || trescX.isEmpty() || trescX.equals("") || trescX.trim().isEmpty() ) {
            _ERROR += "Treść zadania nie może być pusta" + System.lineSeparator();
            trescX = "";
        }
        if (rozwiazanie == null || rozwiazanie.isEmpty() || rozwiazanie.equals("") || rozwiazanie.trim().isEmpty() ) {
            _ERROR += "Rozwiązanie zadania nie może być puste" + System.lineSeparator();
            rozwiazanie = "";
        }
        if (wskazowka == null || wskazowka.isEmpty() || wskazowka.equals("") || wskazowka.trim().isEmpty() ) {
            _ERROR += "Wskazówka zadania nie może być puste" + System.lineSeparator();
            wskazowka = "";
        }
        else {
            _OK = "Edycja zadania przeprowadzona pomyślnie ";
            taskService.updateTask(task,id);
            _ERROR = null;
        }
        redirectAttributes.addFlashAttribute("trescX", trescX);
        redirectAttributes.addFlashAttribute("rozwiazanie", rozwiazanie);
        redirectAttributes.addFlashAttribute("wskazowka", wskazowka);
        redirectAttributes.addFlashAttribute("_ERROR", _ERROR);
        redirectAttributes.addFlashAttribute("_OK", _OK);
        redirectAttributes.addAttribute("type","ZADANIE");

        if(UtilClass.getLoggedInUser().getActiveLessonMaterials() != null){
            Task taskById = taskService.findTaskById(id);
            int index = UtilClass.getLoggedInUser().getActiveLessonMaterials().getTasks().indexOf(taskById);
            UtilClass.getLoggedInUser().getActiveLessonMaterials().getTasks().set(index, taskById);
        }

        if (_ERROR==null) {
            return_text = "redirect:/showTasks";
        }
     return return_text;
    }
    @GetMapping("/deleteTask")
    public String deleteSpecificTest(@RequestParam("id")Long id, RedirectAttributes redirectAttributes){
        Task task= taskRepo.findById(id).orElseThrow(()->new IllegalArgumentException("Nie ma takiego zadania"));
        if(UtilClass.getLoggedInUser() != null && UtilClass.getLoggedInUser().getActiveLessonMaterials()!=null ){
          UtilClass.getLoggedInUser().getActiveLessonMaterials().getTasks().remove(task);
        }
        taskService.deleteByIdAndUser(id,UtilClass.getLoggedInUser());
        String _OK="Zadanie #"+id+" usunięte.";
        redirectAttributes.addFlashAttribute("_OK", _OK);
        redirectAttributes.addAttribute("type","ZADANIE");

        return "redirect:/showTasks";
    }


}


