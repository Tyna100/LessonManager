package com.sdc.LessonManager.services;

import com.sdc.LessonManager.model.entity_IndexController.Task;
import com.sdc.LessonManager.model.entity_IndexController.repo.CategoryRepo;
import com.sdc.LessonManager.model.entity_IndexController.repo.TaskRepo;
import com.sdc.LessonManager.model.lesson.LessonMaterials;
import com.sdc.LessonManager.model.lesson.LessonMaterialsService;
import com.sdc.LessonManager.register_login_logout.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class TaskService {
    private final TaskRepo taskRepo;
    private final CategoryRepo categoryRepo;
    private final LessonMaterialsService lessonMaterialsService;

    public Iterable<Task> getAll() {
        return taskRepo.findAll();
    }

    public Optional<Task> findById(@RequestParam Long index) {
        return taskRepo.findById(index);
    }

    public Task findTaskById(Long index){
        return taskRepo.findById(index).orElseThrow(()-> new IllegalArgumentException("Task with such id doesnt't exist."));}

    public Optional<Task> findByIdKursu(@RequestParam Task task) {
        return taskRepo.findById(task.getZadania_dla_kursu().getId_kurs());
    }

    public Task addTask(@RequestBody Task Task) {
        return taskRepo.save(Task);
    }

    public void updateTask(Task task, Long id) {
        Optional<Task> TaskbyId = findById(id);
        if (TaskbyId.isPresent()) {
            Task task1 = TaskbyId.get();
            task1.setKategoria(task.getKategoria());
            task1.setTresc_zadania(task.getTresc_zadania());
            task1.setRozwiazanie_zadania(task.getRozwiazanie_zadania());
            task1.setWskazowka(task.getWskazowka());
            taskRepo.save(task1);
        }
    }

    public void deleteByIdAndUser(Long index, User user) {
        Task task = findTaskById(index);

        List<LessonMaterials> allByUser = lessonMaterialsService.findAllByUser(user);
        allByUser.stream().forEach(lessonMaterials -> lessonMaterials.removeTask(task));
        allByUser.stream().forEach(lessonMaterials -> lessonMaterialsService.save(lessonMaterials));
        taskRepo.deleteById(index);
    }


    public List<Task> getListOfSelectedTask(String kategoria, Long idGrupy) {
        String category="";
        if (kategoria==null){
            category = categoryRepo.findAll().get(0).getKategoria();
        } else if(kategoria.equals("ALL")){
            return taskRepo.findAll();
        }else{
            category=kategoria;
        }
        List<Task> byKategoria = taskRepo.findAllByKategoriaAndIdgrupa(category, idGrupy);
        return byKategoria;
    }

}