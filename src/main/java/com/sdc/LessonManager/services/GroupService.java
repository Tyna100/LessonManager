package com.sdc.LessonManager.services;

import com.sdc.LessonManager.model.entity_IndexController.Grupa;
import com.sdc.LessonManager.model.entity_IndexController.repo.GrupaRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class GroupService {
    private final GrupaRepo grupaRepo;

    public Iterable<Grupa> findAll() {return grupaRepo.findAll();}

    public Grupa findById(final Long id){
        return this.grupaRepo.findById(id).get();
    }

    public Grupa findByGroupName(String groupname){
        return this.grupaRepo.findByName(groupname);
    }


    public Grupa addgrupa( Grupa grupa) {
        return grupaRepo.save(grupa);
    }

    public void deleteGrupa(Long index) {
        grupaRepo.deleteById(index);
    }



}
