package com.sdc.LessonManager.services;

import com.sdc.LessonManager.model.entity_IndexController.Messenger;
import com.sdc.LessonManager.model.entity_IndexController.MessengerPOJO;
import com.sdc.LessonManager.model.entity_IndexController.repo.MessengerRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class MessengerService {

    private final MessengerRepo messengerRepo;

    public List<Messenger> findAllMessagesById(Long id) {
        ArrayList<Messenger> messengers1 = messengerRepo.findAllByGroupId(id);
        return messengers1;
    }

    public void addMessage (MessengerPOJO messenger, Long id) {
        Messenger msg= new Messenger();
        System.out.println("POJO"+messenger);
        msg.setLocalDateTime(LocalDateTime.now());
        msg.setGroupId(messenger.getGroupId());
        msg.setRole(messenger.getRole());
        msg.setText(messenger.getText());
        msg.setLogin(messenger.getLogin());
        System.out.println("MESSENGER:" +msg);

        messengerRepo.save(msg);
    }

    public LocalDateTime formatData(){
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy:mm:dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        String str = now.format(format);
        return LocalDateTime.parse(str, format);  }


}
