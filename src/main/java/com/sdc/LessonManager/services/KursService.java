package com.sdc.LessonManager.services;

import com.sdc.LessonManager.model.entity_loginController.Kurs;
import com.sdc.LessonManager.model.entity_loginController.repo.KursRepo;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

@Service
public class KursService {
    private KursRepo kursRepo;

    public Iterable<Kurs> getAllKurs(){
    return kursRepo.findAll();};

    public Optional<Kurs> findByIdKursu(@RequestParam Long id) {
        return kursRepo.findById(id);
    }

    public Kurs addKurs(@RequestBody Kurs kurs){
        return kursRepo.save(kurs);}

    public void deleteKurs(@RequestBody Long Index){ kursRepo.deleteById(Index);}

}
